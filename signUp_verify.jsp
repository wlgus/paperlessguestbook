﻿<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy,jxl.*"%>
<%
		 int sizeLimit = 10 * 1024 * 1024 ;
		 String savePath = request.getRealPath("");
		 String temp = "img";
		 String realPath = savePath+"/"+temp;
		 MultipartRequest multi = new MultipartRequest(request, realPath, sizeLimit, "utf-8", new DefaultFileRenamePolicy());
		 String filename = temp+"/"+multi.getFilesystemName("inputOrgLogo");

	    String org_name =multi.getParameter("inputOrgName");
	    String org_homepage = multi.getParameter("inputOrgHomePage");
	    String org_ceo = multi.getParameter("inputOrgCEO");
			String org_tel = multi.getParameter("inputOrgTel");
			String org_admin_name = multi.getParameter("inputOrgAdminName");
			String org_admin_phone= multi.getParameter("inputOrgAdminPhone");
			String org_admin_email= multi.getParameter("inputOrgAdminEmail");
			String org_admin_password = multi.getParameter("inputOrgAdminPassword");
			String org_admin_password2 = multi.getParameter("inputOrgAdminPassword2");

			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
			String org_update = formatter.format(new java.util.Date());
			Connection myConn = null;
			PreparedStatement pstmt = null;
			String mySQL = null;
			String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
			String db_user = "intern";
			String db_password = "password";
			String Driver="org.mariadb.jdbc.Driver";
			if(!org_admin_password.equals(org_admin_password2)) {
			%>
			<script>
				alert("비밀번호를 체크하세요.");
				location.href="signUp.jsp";
			</script>
			<%
				return;
			}
			try{
				Class.forName(Driver);
				myConn = DriverManager.getConnection(db_url, db_user, db_password);
				}catch(SQLException ex){
				System.err.println("SQLException: " + ex.getMessage());
			}

	try{
			mySQL ="Insert into Organization(ORG_NAME, ORG_HOMEPAGE, ORG_IMAGE, ORG_CEO, ORG_TEL, ORG_ADMIN_NAME, ORG_ADMIN_PASS, ORG_ADMIN_EMAIL, ORG_ADMIN_PHONE, ORG_UPDATE) values(?,?,?,?,?,?,?,?,?, ?)";
			pstmt = myConn.prepareStatement(mySQL);

			pstmt.setString(1, org_name);
			pstmt.setString(2, org_homepage);
			pstmt.setString(3, filename);
			pstmt.setString(4, org_ceo);
			pstmt.setString(5, org_tel);
			pstmt.setString(6, org_admin_name);
			pstmt.setString(7, org_admin_password);
			pstmt.setString(8, org_admin_email);
			pstmt.setString(9, org_admin_phone);
			pstmt.setString(10, org_update);

			if(pstmt.executeUpdate() != 0){
			%>
			<script>
				alert("회원가입에 성공하였습니다.");
				location.href = "signIn.jsp";
			</script>
			<%
			}
			else{
			%>
			<script>
				alert("회원가입에 실패하였습니다.");
				location.href="signIn.jsp";
			</script>
			<%
			}
		} catch(Exception ex){
			ex.printStackTrace();
			System.err.println("SQLException: " + ex.getMessage());
		}finally{
			myConn.close(); pstmt.close();
		}
%>
