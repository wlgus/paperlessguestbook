<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>
<%@ include file = "session.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>회원사검색 &middot; FORCS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/making.css" rel="stylesheet">
  <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-searchMembership {
      max-width: 900px;
      padding: 0px;
      margin: 20px auto;
      background-color: #fff;
      border: 1px solid #e5e5e5;
      -webkit-border-top-right-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

    .form-searchMembership .form-searchMembership-heading {
      color: white;
      padding: 50px 0;
      margin-top: 0px;
      margin-bottom: 35px;
      background-color: rgb(33, 47, 61);
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
    }

    .form-searchMembership input[type="text"],
    .form-searchMembership input[type="date"]{
      font-size: 13px;
      height: 38px;
      margin-bottom: 15px;
    }
  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
  <div class="container-fluid">
    <form name="searchMembershipForm" class="form-searchMembership " method="post">

          <div class="row center-block">
            <button type="button" class="arrow-btn btn-link" onclick="location.href='manageMembership.jsp'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
            <h3 class="form-searchMembership-heading text-center">
              회원사 검색
            </h3>
          </div>

          <div class="row center-block">
            <div class="col-sm-12 border">
              <h4><b>회원사 검색</b></h4><span class="space"></span>

              <form class="form-searchMembership" method="post" action="searchMembership_result.jsp">
                <div class="form-group col-sm-12">
                  <label for="inputMembershipName" class="col-sm-2 control-label col-xs-12">회원사명</label>
                  <div class="col-sm-8 col-xs-8">
                    <input type="text" class="form-control" name="inputMembershipName" placeholder="회원사명">
                  </div>
                  <div class="col-sm-2 col-xs-4">
                    <button type="button" class="btn btn-sm btn-primary space-remove btn-block" onclick="search()">검색</button>
                  </div>
                </div>
                <span class="space"></span>
              </form>

              <span class="space"></span>

              <div class="" align="right">
                <button type="button" class="btn btn-link btn-sm" onclick="edit()">회원사 편집 <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
              </div>
              <br>
              <div class="table-responsive">
                <table class="table table-hover table-bordered">
                  <thead>
                    <tr>
                      <th class="text-center"><b>No.</b></th>
                      <th class="text-center"><b>회원사 레벨</b></b></th>
                      <th class="text-center"><b>회사</b></th>
                      <th class="text-center"><b>주력품목</b></th>
                      <th class="text-center"><b>구분</b></th>
                      <th class="text-center"><b>회원수</b></th>
                      <th class="text-center"><b>선택</b></th>
                      <th class="text-center"><b>홈페이지</b></th>
                  </thead>

                  <%
                    String membership_name =new String(request.getParameter("inputMembershipName").getBytes("8859_1"),"utf-8");

                    Connection myConn = null;
                    PreparedStatement pstmt = null;
                    Statement stmt = null;
                    ResultSet myResultSet;
                    ResultSet rs2;
                    String mySQL = null;
                    String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
                    String db_user = "intern";
                    String db_password = "password";
                    String Driver="org.mariadb.jdbc.Driver";
                    int number=0;

                    try {
                      Class.forName(Driver);
                      myConn = DriverManager.getConnection(db_url, db_user, db_password);
                    } catch(SQLException ex) {
                        System.err.println("SQLException: " + ex.getMessage());
                    }
                    try{

                      mySQL = "select MEMBERSHIP_NAME, MEMBERSHIP_LEVEL, HOMEPAGE, TYPE, MAIN_ITEM from Membership where ORG_ID = '"+ session_id +"'  and MEMBERSHIP_NAME like ?";
                      pstmt = myConn.prepareStatement(mySQL);
                      pstmt.setString(1, "%" + membership_name + "%");

                      myResultSet = pstmt.executeQuery();

                      while(myResultSet.next()){
                        number++;
                        membership_name = myResultSet.getString("MEMBERSHIP_NAME");
                        String membership_level = myResultSet.getString("MEMBERSHIP_LEVEL");
                        String homepage = myResultSet.getString("HOMEPAGE");
                        String type = myResultSet.getString("TYPE");
                        String main_item = myResultSet.getString("MAIN_ITEM");

                       mySQL = "select * from Member where ORG_ID = '" + session_id + "' and MEMBERSHIP_NAME = '" + membership_name +"'" ;
                       stmt=myConn.createStatement();
                       rs2 = stmt.executeQuery(mySQL);
                       rs2.last();
                       int mem_number = rs2.getRow();
                      %>
                        <tbody>
                          <tr>
                          <td class="text-center"><%=number%></td>
                          <td class="text-center"><%=membership_level%></td>
                          <td class="text-center"><%=membership_name%></td>
                          <td class="text-center"><%=main_item%></td>
                          <td class="text-center"><%=type%></td>
                          <td class="text-center"><%=mem_number%></td>
                          <td class="text-center"><input type="radio" name="editMembership" value="<%=membership_name%>"></td>
                          <td class="text-center"><a href="<%=homepage%>" name="MembershipHomePage" class="btn-link">이동</a></td>
                          </tr>
                        </tbody>
                        <%
                          }
                        } catch(Exception ex){
                              ex.printStackTrace();
                              System.err.println("SQLException: " + ex.getMessage());
                        } finally{
                           myConn.close(); pstmt.close();
                        }
                        %>
                </table>
              </div>
              <span class="space"></span>
            </div>
            </div>
          </div>

    </form>
  </div>
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap-transition.js"></script>
  <script src="js/bootstrap-alert.js"></script>
  <script src="js/bootstrap-modal.js"></script>
  <script src="js/bootstrap-dropdown.js"></script>
  <script src="js/bootstrap-scrollspy.js"></script>
  <script src="js/bootstrap-tab.js"></script>
  <script src="js/bootstrap-tooltip.js"></script>
  <script src="js/bootstrap-popover.js"></script>
  <script src="js/bootstrap-button.js"></script>
  <script src="js/bootstrap-collapse.js"></script>
  <script src="js/bootstrap-carousel.js"></script>
  <script src="js/bootstrap-typeahead.js"></script>
<script>
  function search(){
    document.searchMembershipForm.action="searchMembership_result.jsp";
    document.searchMembershipForm.method="post";
    document.searchMembershipForm.submit();
  }
  function edit(){
    var radio_btn = document.getElementsByName("editMembership");
     var radio_btn_check = 0;
     for(var i = 0; i<radio_btn.length; i++){
         if(radio_btn[i].checked==true){
             radio_btn_check++;
         }
     }
     if(radio_btn_check==0) {
       alert("편집할 회원사를 선택하세요.");
       return false;
     }
     else {
        document.searchMembershipForm.action="editMembership.jsp";
        document.searchMembershipForm.method="post";
        document.searchMembershipForm.submit();
      }
  }
</script>
 </body>
</html>
