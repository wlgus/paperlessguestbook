﻿﻿<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>
<% response.setCharacterEncoding("UTF-8"); %>

<%
	String org_id =new String(request.getParameter("inputOrgName").getBytes("8859_1"),"utf-8");
	String original_org_id =new String(request.getParameter("inputOriginalOrgName").getBytes("8859_1"),"utf-8");
  String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
  String db_user = "intern";
  String db_password = "password";
  String mySQL = null;
  PreparedStatement pstmt = null;
  Connection myConn=null;
  Statement stmt=null;
  String Driver="org.mariadb.jdbc.Driver";

  try{
		Class.forName(Driver);
		myConn = DriverManager.getConnection(db_url, db_user, db_password);
	}catch(SQLException ex){
			System.err.println("SQLException: " + ex.getMessage());
	}
  try{
		mySQL = "select ORG_NAME from Organization where ORG_NAME = ?";
		pstmt = myConn.prepareStatement(mySQL);
		pstmt.setString(1, org_id);
		ResultSet myResultSet = pstmt.executeQuery();
		if(myResultSet.next() && !org_id.equals(original_org_id)){
%>
<script>
		alert("중복된 아이디입니다.");
		window.opener.no();
		window.close();
</script>
<%
		}
		else{
%>
<script>
		alert("사용하실 수 있습니다.");
	  window.opener.ok();
		window.opener.ok_id();
		window.close();
</script>
<%
}} catch(Exception ex){
			ex.printStackTrace();
			System.err.println("SQLException: " + ex.getMessage());
	}finally{
		myConn.close(); pstmt.close();
	}
%>
