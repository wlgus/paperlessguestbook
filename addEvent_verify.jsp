<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy,jxl.*"%>
<%@ include file = "session.jsp"%>

<%
  int sizeLimit = 10 * 1024 * 1024 ;
  String savePath = request.getRealPath("");
  String temp = "img";
  String realPath = savePath+"/"+temp;
  MultipartRequest multi = new MultipartRequest(request, realPath, sizeLimit, "utf-8", new DefaultFileRenamePolicy());
  String filename = temp+"/"+multi.getFilesystemName("inputEventPhoto");

  String org_id = (String)session.getAttribute("id");
  String event_type = multi.getParameter("inputEventType");
  String event_homepage = multi.getParameter("inputEventHomePage");
  String event_p_d = multi.getParameter("inputEventPreRegistrationDate");
  String event_p_t = multi.getParameter("inputEventPreRegistrationTime");
  String event_number = multi.getParameter("inputEventNum");
  String event_name = multi.getParameter("inputEventName");
  String event_s_d = multi.getParameter("inputEventStartDate");
  String event_s_t = multi.getParameter("inputEventStartTime");
  String event_e_d = multi.getParameter("inputEventEndDate");
  String event_e_t = multi.getParameter("inputEventEndTime");
  String event_order = multi.getParameter("inputEventSortingOrder");
  String event_host = multi.getParameter("inputEventHost");
  String event_topic = multi.getParameter("inputEventSubject");
  String event_detail = multi.getParameter("inputEventComment");
  String event_supervision = multi.getParameter("inputEventManagement");
  String event_place = multi.getParameter("inputEventPlace");
  String event_status = "진행중";

  java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
 	String event_update = formatter.format(new java.util.Date());
  Connection myConn = null;
  PreparedStatement pstmt = null;
  String mySQL = null;
  String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
  String db_user = "intern";
  String db_password = "password";
  String Driver="org.mariadb.jdbc.Driver";

  try{
    Class.forName(Driver);
    myConn = DriverManager.getConnection(db_url, db_user, db_password);
  }catch(SQLException ex){
      System.err.println("SQLException: " + ex.getMessage());
  }
  try{
      mySQL ="Insert into Event(ORG_ID, TITLE, EVENT_TYPE, EVENT_HOMEPAGE, EVENT_IMAGE, EVENT_S_D, EVENT_S_T, EVENT_E_D, EVENT_E_T, PRE_CLOSING_D, PRE_CLOSING_T, PEOPLE_NUMBER, EVENT_STATUS, GUESTBOOK_ORDER, EVENT_UPDATE, TOPIC, EVENT_HOST, SUPERVISION, EVENT_PLACE, EVENT_DETAIL) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

      pstmt = myConn.prepareStatement(mySQL);

      pstmt.setString(1, org_id);
    	pstmt.setString(2, event_name);
    	pstmt.setString(3, event_type);
    	pstmt.setString(4, event_homepage);
    	pstmt.setString(5, filename);
    	pstmt.setString(6, event_s_d);
    	pstmt.setString(7, event_s_t);
    	pstmt.setString(8, event_e_d);
    	pstmt.setString(9, event_e_t);
    	pstmt.setString(10, event_p_d);
    	pstmt.setString(11, event_p_t);
    	pstmt.setString(12, event_number);
    	pstmt.setString(13, event_status);
    	pstmt.setString(14, event_order);
    	pstmt.setString(15, event_update);
    	pstmt.setString(16, event_topic);
    	pstmt.setString(17, event_host);
    	pstmt.setString(18, event_supervision);
    	pstmt.setString(19, event_place);
    	pstmt.setString(20, event_detail);

      if(pstmt.executeUpdate() != 0){
  %>
  <script>
  			alert("행사등록이 완료되었습니다.");
  			location.href="manageEventOngoing.jsp";
  </script>
  <%
  		}
  		else{
   %>
    <script>
    			alert("행사등록이 불가능합니다.");
    			location.href="addEvent.jsp";
    </script>
   <%
  		}
  	} catch(Exception ex){
  			ex.printStackTrace();
  			System.err.println("SQLException: " + ex.getMessage());
  	} finally {
  		myConn.close(); pstmt.close();
  	}
  %>
