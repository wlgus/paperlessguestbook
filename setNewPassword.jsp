<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>

<%  String session_email= ((String)session.getAttribute("email")); %>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>비밀번호찾기 &middot; FORCS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/making.css" rel="stylesheet">
  <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-setNewPassword {
      max-width: 900px;
      padding: 0px;
      margin: 20px auto;
      background-color: #fff;
      border: 1px solid #e5e5e5;
      -webkit-border-top-right-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

    .form-setNewPassword .form-setNewPassword-heading {
      color: white;
      padding: 50px 0;
      margin-top: 0px;
      margin-bottom: 35px;

      background-color: rgb(33, 47, 61);
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
    }

    .form-setNewPassword input[type="text"],
    .form-setNewPassword input[type="password"],
    .form-setNewPassword input[type="file"],
    .form-setNewPassword input[type="tel"],
    .form-setNewPassword input[type="email"] {
      font-size: 15px;
      height: auto;
      margin-bottom: 25px;
      padding: 7px 9px;
    }

    .form-setNewPassword-2 {
      padding: 30px 20px;
      margin: 20px auto 40px;
      background-color: #f5f5f5;
      border: 1px solid #e5e5e5;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
  <div class="container-fluid">
    <div class="form-setNewPassword ">

          <div class="row center-block">
            <button type="button" class="arrow-btn btn-link" onclick="location.href='findPassword.jsp'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
            <h3 class="form-setNewPassword-heading text-center">
              비밀번호 재설정
            </h3>
          </div>

          <div class="row center-block">
            <div class="col-sm-offset-3 col-sm-6 border">
              <form action="setNewPassword_verify.jsp" class="form-setNewPassword-2" name="setNewPasswordfForm">
                <center><h4 class="hidden-xs"><b>새로운 비밀번호를 입력해주세요</b></h4></center>
                <center><h5 class="visible-xs"><b>새로운 비밀번호를 입력해주세요</b></h5></center>
                <span class="space"></span>
                <input type="text" name="sessionEmail" class="hidden" value="<%=session_email%>">
                <input type="password" name="newPassword1" class="input-block-level" placeholder="새 비밀번호 입력">
                <input type="password" name="newPassword2" class="input-block-level" placeholder="새 비밀번호 확인">
                <button type="submit" class="btn btn-large btn-primary btn-block" onclick="return check()">변경</button>
                <span class="space"></span>
                <center><img class="img-responsive" src="img/logo.png" alt="logo"></center>
              </form>
            </div>
            </div>
          </div>

    </div>
  </div>
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap-transition.js"></script>
  <script src="js/bootstrap-alert.js"></script>
  <script src="js/bootstrap-modal.js"></script>
  <script src="js/bootstrap-dropdown.js"></script>
  <script src="js/bootstrap-scrollspy.js"></script>
  <script src="js/bootstrap-tab.js"></script>
  <script src="js/bootstrap-tooltip.js"></script>
  <script src="js/bootstrap-popover.js"></script>
  <script src="js/bootstrap-button.js"></script>
  <script src="js/bootstrap-collapse.js"></script>
  <script src="js/bootstrap-carousel.js"></script>
  <script src="js/bootstrap-typeahead.js"></script>
  <script type="text/javascript">
  function check(){
    if(document.setNewPasswordfForm.newPassword1.value == "") {
        alert("비밀번호를 입력해주세요.");
        return false;
    }
  }
  </script>
 </body>
</html>
