<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>
<%@ include file = "session.jsp"%>

<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="utf-8">
 <title>회원사관리 &middot; FORCS</title>
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta name="description" content="">
 <meta name="author" content="">

 <link href="css/bootstrap.css" rel="stylesheet">
 <link href="css/making.css" rel="stylesheet">
 <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
 <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
 <style type="text/css">
   body {
     padding-top: 40px;
     padding-bottom: 40px;
     background-color: #f5f5f5;
   }

   .form-manageMembership {
     max-width: 900px;
     padding: 0px;
     margin: 20px auto;
     background-color: #fff;
     border: 1px solid #e5e5e5;
     -webkit-border-top-right-radius: 5px;
     -moz-border-radius: 5px;
     border-radius: 5px;
     -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
     -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
     box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
   }

   .form-manageMembership .form-manageMembership-heading {
     color: white;
     padding: 50px 0;
     margin-top: 0px;
     margin-bottom: 35px;
     background-color: rgb(33, 47, 61);
     border-top-right-radius: 5px;
     border-top-left-radius: 5px;
   }

 </style>
 <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
 <%!
    public Integer toInt(String x){
       int a = 0;
       try{
          a = Integer.parseInt(x);
       }catch(Exception e){}
       return a;
    }
 %>

 <div class="container-fluid">
   <div class="form-manageMembership ">

         <div class="row center-block">
           <button type="button" class="arrow-btn btn-link" onclick="location.href='main.jsp'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
           <button type="button" class="search-btn btn-link" onclick="location.href='searchMembership.jsp'"><i class="fa fa-search" aria-hidden="true"></i></button>
           <h3 class="form-manageMembership-heading text-center">
             회원사 관리
           </h3>
         </div>

         <div class="row center-block">
           <div class="col-sm-12 border">
             <h4><b>회원사 목록</b></h4><span class="space"></span>
             <div class="" align="right">
               <form class="" name="edit" action="editMembership.jsp" method="post">
              <button type="button" class="btn btn-link btn-sm" onclick="location.href='addMembership.jsp'">회원사 등록 <i class="fa fa-building-o" aria-hidden="true"></i></button>
               <button type="submit" class="btn btn-link btn-sm" onclick="return check()">회원사 편집 <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
             </div>
             <br>
             <div class="table-responsive">
               <table class="table table-hover table-bordered">
                 <thead>
                   <tr>
                     <th class="text-center"><b>No.</b></th>
                     <th class="text-center"><b>회원사 레벨</b></b></th>
                     <th class="text-center"><b>회사</b></th>
                     <th class="text-center"><b>주력품목</b></th>
                     <th class="text-center"><b>구분</b></th>
                     <th class="text-center"><b>회원수</b></th>
                     <th class="text-center"><b>선택</b></th>
                     <th class="text-center"><b>홈페이지</b></th>
                   </tr>
                 </thead>
                   <%
                   int pageno = toInt(request.getParameter("pageno"));
                   if(pageno<1)
                   {
                     pageno =1;
                   }

                     Connection myConn = null;
                     Statement stmt = null;
                     PreparedStatement pstmt = null;
                     ResultSet rs;
                     ResultSet rs2;
                     ResultSet paging;

                     String mySQL = null;
                     String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
                     String db_user = "intern";
                     String db_password = "password";
                     String Driver="org.mariadb.jdbc.Driver";
                     int number =0;

                     int total_record=0;
                    int page_per_record_cnt=10;
                     int page_group=0; // 총 몇개의 페이지인지
                     int view_page = 0; // 보여주기 시작할 위치

                     if(pageno == 1)
                     {
                       view_page =0;
                     }else{
                       view_page= (pageno-1)*page_per_record_cnt;
                     }

                   try{
                       Class.forName(Driver);
                       myConn = DriverManager.getConnection(db_url, db_user, db_password);
                       stmt = myConn.createStatement();
                     }catch(SQLException ex){
                       System.err.println("SQLException: " + ex.getMessage());
                     }

                     mySQL = "select * from Membership where ORG_ID = '" + session_id + "'";
                     paging = stmt.executeQuery(mySQL);
                     paging.last();
                     total_record = paging.getRow(); // 전체 레코드 수

                     mySQL = "select MEMBERSHIP_NAME, MEMBERSHIP_LEVEL, HOMEPAGE, LOGO_IMAGE, TYPE, MAIN_ITEM from Membership where ORG_ID = '" + session_id + "' limit "+view_page+","+page_per_record_cnt+"";
                     rs = stmt.executeQuery(mySQL);

                     if(rs != null) {
                     while(rs.next()){
                       number++;
                       String membership_NAME = rs.getString("MEMBERSHIP_NAME");
                       String membership_level = rs.getString("MEMBERSHIP_LEVEL");
                       String homepage = rs.getString("HOMEPAGE");
                       String logo_image = rs.getString("LOGO_IMAGE");
                       String type = rs.getString("TYPE");
                       String main_item = rs.getString("MAIN_ITEM");

                      mySQL = "select * from Member where ORG_ID = '" + session_id + "' and membership_NAME = '" + membership_NAME +"'" ;
                      rs2 = stmt.executeQuery(mySQL);
                      rs2.last();
                      int mem_number = rs2.getRow();

                   %>
                   <tbody>
                     <tr>
                       <td class="text-center"><%=number%></td>
                       <td class="text-center"><%=membership_level%></td>
                       <td class="text-center"><%=membership_NAME%></td>
                       <td class="text-center"><%=main_item%></td>
                       <td class="text-center"><%=type%></td>
                       <td class="text-center"><%=mem_number%></td>
                       <td class="text-center"><input type="radio" name="editMembership" value="<%=membership_NAME%>"></td>
                       <td class="text-center"><a href="<%=homepage%>" name="MembershipHomePage" class="btn-link">이동</a></td>
                     </tr>

                   </tbody>
                     <%
                   }
                 } rs.close();
                  stmt.close();
                  if(total_record % page_per_record_cnt != 0) // 남는 레코드 수 존재
                  {
                    page_group = (total_record/page_per_record_cnt)+1;
                  }else{
                    page_group = total_record / page_per_record_cnt;
                  }
                   int group_per_page_cnt  = 5;     //페이지 당 보여줄 번호 수

                   int record_end_no = pageno*page_per_record_cnt;
                   int record_start_no = record_end_no-(page_per_record_cnt-1);
                   if(record_end_no>total_record){
                      record_end_no = total_record;
                   }

                   int total_page = total_record / page_per_record_cnt + (total_record % page_per_record_cnt>0 ? 1 : 0);
                   if(pageno>total_page){
                      pageno = total_page;
                   }
                   int group_no = pageno/group_per_page_cnt+( pageno%group_per_page_cnt>0 ? 1:0);
                   int page_eno = group_no*group_per_page_cnt;
                   int page_sno = page_eno-(group_per_page_cnt-1);

                   if(page_sno<1)
                    {
                      page_sno=1;
                    }

                   if(page_eno>total_page){
                      page_eno=total_page;
                   }

                   int prev_pageno = page_sno-group_per_page_cnt;
                   int next_pageno = page_sno+group_per_page_cnt;
                   if(prev_pageno<1){
                      prev_pageno=1;
                   }
                   if(next_pageno>total_page){
                      next_pageno=total_page/group_per_page_cnt*group_per_page_cnt+1;
                   }
                      %>
                      </form>
                      </table>
                      <div class="text-center">
                        <ul class="pagination">
                          <li><a href="manageMembership.jsp?pageno=<%=prev_pageno%>">이전</a></li>
                          <%for(int i =page_sno;i<=page_eno;i++){%>
                            <li><a href="manageMembership.jsp?pageno=<%=i %>">
                              <%if(pageno == i){ %>
                                <%=i %>
                              <%}else{ %>
                                <%=i %>
                              <%} %>
                            </a></li>
                          <%--   콤마    --%>
                            <%if(i<page_eno){ %>
                            <%} %>
                          <%} %>
                          <li><a href="manageMembership.jsp?pageno=<%=next_pageno%>" >다음</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  </div>
                </div>
          </div>
        </div>
 <script src="js/jquery.js"></script>
 <script src="js/bootstrap-transition.js"></script>
 <script src="js/bootstrap-alert.js"></script>
 <script src="js/bootstrap-modal.js"></script>
 <script src="js/bootstrap-dropdown.js"></script>
 <script src="js/bootstrap-scrollspy.js"></script>
 <script src="js/bootstrap-tab.js"></script>
 <script src="js/bootstrap-tooltip.js"></script>
 <script src="js/bootstrap-popover.js"></script>
 <script src="js/bootstrap-button.js"></script>
 <script src="js/bootstrap-collapse.js"></script>
 <script src="js/bootstrap-carousel.js"></script>
 <script src="js/bootstrap-typeahead.js"></script>

 <script type="text/javascript">
 function check(){
   var radio_btn = document.getElementsByName("editMembership");

        var radio_btn_check = 0;
        for(var i = 0; i<radio_btn.length; i++){
            //만약 라디오 버튼이 체크가 되어있다면 true
            if(radio_btn[i].checked==true){
                //라디오 버튼 값
                //alert(radio_btn[i].value);
                //라디오 버튼이 체크되면 radio_btn_check를 1로 만들어준다.
                radio_btn_check++;
            }
        }
        if(radio_btn_check==0)
        {
          alert("편집할 회원사를 선택하세요.");
          return false;
        }
 }
 </script>
 </body>
</html>
