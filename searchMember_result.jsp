<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>
<%@ include file = "session.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>행사검색 &middot; FORCS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/making.css" rel="stylesheet">
  <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-searchMember {
      max-width: 900px;
      padding: 0px;
      margin: 20px auto;
      background-color: #fff;
      border: 1px solid #e5e5e5;
      -webkit-border-top-right-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

    .form-searchMember .form-searchMember-heading {
      color: white;
      padding: 50px 0;
      margin-top: 0px;
      margin-bottom: 35px;
      background-color: rgb(33, 47, 61);
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
    }

    .form-searchMember input[type="text"],
    .form-searchMember input[type="date"]{
      font-size: 13px;
      height: 38px;
      margin-bottom: 15px;
    }
  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
  <div class="container-fluid">
    <form name="searchMemberForm" class="form-searchMember " method="post">

          <div class="row center-block">
            <button type="button" class="arrow-btn btn-link" onclick="location.href='manageMember.jsp'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
            <h3 class="form-searchMember-heading text-center">
              회원 검색
            </h3>
          </div>

          <div class="row center-block">
            <div class="col-sm-12 border">
              <h4><b>회원 검색</b></h4><span class="space"></span>

              <form class="form-searchMember" method="post" action="searchMember_result.jsp">
                <div class="form-group col-sm-12">
                  <label for="inputMemberName" class="col-sm-2 control-label col-xs-12">회원명</label>
                  <div class="col-sm-8 col-xs-8">
                    <input type="text" class="form-control" name="inputMemberName" placeholder="회원명">
                  </div>
                  <div class="col-sm-2 col-xs-4">
                    <button type="button" class="btn btn-sm btn-primary space-remove btn-block" onclick="search()">검색</button>
                  </div>
                </div>
                <span class="space"></span>
              </form>

              <span class="space"></span>

              <div class="" align="right">
                <button type="button" class="btn btn-link btn-sm" onclick="edit()">회원 편집 <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
              </div>
              <br>
              <div class="table-responsive">
                <table class="table table-hover table-bordered">
                  <thead>
                    <tr>
                      <th class="text-center"><b>No.</b></th>
                      <th class="text-center"><b>회원사 레벨</b></b></th>

                      <th class="text-center"><b>회사</b></th>
                      <th class="text-center"><b>이름</b></th>
                      <th class="text-center"><b>직위</b></th>
                      <th class="text-center"><b>연락처</b></th>
                      <th class="text-center"><b>선택</b></th>
                    </tr>
                  </thead>
                  <%
                    String member_name =new String(request.getParameter("inputMemberName").getBytes("8859_1"),"utf-8");
                    Connection myConn = null;
                    PreparedStatement pstmt = null;
                    ResultSet myResultSet;
                    String mySQL = null;
                    String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
                    String db_user = "intern";
                    String db_password = "password";
                    String Driver="org.mariadb.jdbc.Driver";
                    int num=0;

                    try {
                      Class.forName(Driver);
                      myConn = DriverManager.getConnection(db_url, db_user, db_password);
                    } catch(SQLException ex) {
                        System.err.println("SQLException: " + ex.getMessage());
                    }
                    try{

                      mySQL = "select MEMBERSHIP_LEVEL, Membership.MEMBERSHIP_NAME, MEM_NAME, MEM_POSITION, MEM_PHONE from Membership, Member where Membership.ORG_ID = '"+ session_id +"' and Membership.MEMBERSHIP_NAME = Member.MEMBERSHIP_NAME and MEM_NAME like ?";
                      pstmt = myConn.prepareStatement(mySQL);
                      pstmt.setString(1, "%" + member_name + "%");

                      myResultSet = pstmt.executeQuery();

                      while(myResultSet.next()){
                        num++;
                        String membership_name = myResultSet.getString("MEMBERSHIP_NAME");
                        String membership_level = myResultSet.getString("MEMBERSHIP_LEVEL");
                        String mem_name = myResultSet.getString("MEM_NAME");
                        String mem_position = myResultSet.getString("MEM_POSITION");
                        String mem_phone = myResultSet.getString("MEM_PHONE");
                      %>
                        <tbody>
                          <tr>
                            <td class="text-center"><%=num%></td>
                            <td class="text-center"><%=membership_level%></td>
                            <td class="text-center"><%=membership_name%></td>
                            <td class="text-center"><%=mem_name%></td>
                            <td class="text-center"><%=mem_position%></td>
                            <td class="text-center"><%=mem_phone%></td>
                            <td class="text-center"><input type="radio" name="editMember" value="<%=mem_name%>"></td>
                          </tr>
                        </tbody>
                        <%
                          }
                        } catch(Exception ex){
                              ex.printStackTrace();
                              System.err.println("SQLException: " + ex.getMessage());
                        } finally{
                           myConn.close(); pstmt.close();
                        }
                        %>
                </table>
              </div>
              <span class="space"></span>
            </div>
            </div>
          </div>

    </form>
  </div>
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap-transition.js"></script>
  <script src="js/bootstrap-alert.js"></script>
  <script src="js/bootstrap-modal.js"></script>
  <script src="js/bootstrap-dropdown.js"></script>
  <script src="js/bootstrap-scrollspy.js"></script>
  <script src="js/bootstrap-tab.js"></script>
  <script src="js/bootstrap-tooltip.js"></script>
  <script src="js/bootstrap-popover.js"></script>
  <script src="js/bootstrap-button.js"></script>
  <script src="js/bootstrap-collapse.js"></script>
  <script src="js/bootstrap-carousel.js"></script>
  <script src="js/bootstrap-typeahead.js"></script>
  <script>
      function search(){
        document.searchMemberForm.action="searchMember_result.jsp";
        document.searchMemberForm.method="post";
        document.searchMemberForm.submit();
      }
      function edit(){
        var radio_btn = document.getElementsByName("editMember");
         var radio_btn_check = 0;
         for(var i = 0; i<radio_btn.length; i++){
             if(radio_btn[i].checked==true){
                 radio_btn_check++;
             }
         }
         if(radio_btn_check==0) {
           alert("편집할 회원을 선택하세요.");
           return false;
         }
         else {
            document.searchMemberForm.action="editMember.jsp";
            document.searchMemberForm.method="post";
            document.searchMemberForm.submit();
          }
      }
    </script>
 </body>
</html>
