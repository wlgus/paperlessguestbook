<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>
<%@ include file = "session.jsp"%>

<%
String org_name =new String(request.getParameter("inputOrgName").getBytes("8859_1"),"utf-8");
String org_homepage = new String(request.getParameter("inputOrgHomePage").getBytes("8859_1"),"utf-8");
String org_ceo = new String(request.getParameter("inputOrgCEO").getBytes("8859_1"),"utf-8");
String org_image = new String(request.getParameter("inputOrgLogo").getBytes("8859_1"),"utf-8");
String org_tel = new String(request.getParameter("inputOrgTel").getBytes("8859_1"),"utf-8");
String org_admin_name = new String(request.getParameter("inputOrgAdminName").getBytes("8859_1"),"utf-8");
String org_admin_phone= new String(request.getParameter("inputOrgAdminPhone").getBytes("8859_1"),"utf-8");
String org_admin_email= new String(request.getParameter("inputOrgAdminEmail").getBytes("8859_1"),"utf-8");
String org_admin_password = new String(request.getParameter("inputOrgAdminPassword").getBytes("8859_1"),"utf-8");
String org_admin_password2 = new String(request.getParameter("inputOrgAdminPassword2").getBytes("8859_1"),"utf-8");

java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
String org_update = formatter.format(new java.util.Date());

Connection myConn = null;
PreparedStatement pstmt = null;

String mySQL = null;
String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
String db_user = "intern";
String db_password = "password";
String Driver="org.mariadb.jdbc.Driver";

try {
     Class.forName(Driver);
     myConn = DriverManager.getConnection(db_url, db_user, db_password);
}catch(SQLException ex) {
         System.err.println("SQLException: " + ex.getMessage());
}

if(!org_admin_password.equals(org_admin_password2)) {
%>
<script>
  alert("비밀번호를 체크하세요.");
  location.href="orgInfo.jsp";
</script>
<%
 return;
}
try{

   mySQL = "Update Organization Set ORG_NAME = ?, ORG_HOMEPAGE = ?, ORG_IMAGE = ?, ORG_CEO = ?, ORG_TEL = ?, ORG_ADMIN_NAME = ?, ORG_ADMIN_PASS = ?, ORG_ADMIN_EMAIL = ?, ORG_ADMIN_PHONE = ?, ORG_UPDATE = ? where ORG_ID = '" + session_id +"'";

     pstmt = myConn.prepareStatement(mySQL);

     pstmt.setString(1, org_name);
     pstmt.setString(2, org_homepage);
     pstmt.setString(3, org_image);
     pstmt.setString(4, org_ceo);
     pstmt.setString(5, org_tel);
     pstmt.setString(6, org_admin_name);
     pstmt.setString(7, org_admin_password);
     pstmt.setString(8, org_admin_email);
     pstmt.setString(9, org_admin_phone);
     pstmt.setString(10, org_update);

     if(pstmt.executeUpdate() != 0){
%>
<script>
        alert("정보 수정을 완료하였습니다.");
        location.href = "main.jsp";
</script>
<%
     }
     else{
%>
<script>
        alert("정보 수정에 실패하였습니다.");
        location.href="main.jsp";
</script>
<%
     }

  } catch(Exception ex){
        ex.printStackTrace();
        System.err.println("SQLException: " + ex.getMessage());
  }finally{
     myConn.close(); pstmt.close();
  }
%>
