﻿﻿﻿<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>기관가입&middot; FORCS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/making.css" rel="stylesheet">
  <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-signup {
      max-width: 900px;
      padding: 19px;
      margin: 20px auto;
      background-color: #fff;
      border: 1px solid #e5e5e5;
      -webkit-border-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

    .form-signup .form-signup-heading {
      margin-bottom: 25px;
    }

    .form-signup input[type="text"],
    .form-signup input[type="password"],
    .form-signup input[type="file"],
    .form-signup input[type="tel"],
    .form-signup input[type="email"] {
      font-size: 15px;
      height: auto;
      margin-bottom: 25px;
      padding: 7px 9px;
    }

  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script>
 $(function(){
  $('#inputOrgAdminPassword').keyup(function(){
   $('font[passwordCheckText]').text('');
  }); //#user_pass.keyup

  $('#inputOrgAdminPassword2').keyup(function(){
   if($('#inputOrgAdminPassword').val()!=$('#inputOrgAdminPassword2').val()){
    $('font[passwordCheckText]').text('');
    $('font[passwordCheckText]').html("암호틀림");
   }else{
    $('font[passwordCheckText]').text('');
    $('font[passwordCheckText]').html("암호맞음");
   }
  }); //#chpass.keyup
 });
</script>

<body>
  <div class="container-fluid">
    <form name="f2" action="signUp_verify.jsp" class="form-signup " method= "post" enctype="multipart/form-data">

          <div class="row center-block">
            <div class="page-header">
              <h3 class="form-signup-heading text-center">기관가입 <small class="hidden-xs">Organiztion Sign Up</small></h3>
              <hr>
            </div>
          </div>

          <div class="row center-block">
            <div class="col-sm-6 border">
              <h4><b>기관 정보</b></h4><span class="space"></span>

              <div class="form-group">
                  <label for="inputOrgName" class="col-sm-3 control-label col-xs-12"><span class="red-color-text">*</span>기관이름</label>
                  <div class="col-sm-7 col-xs-10">
                    <input type="text" class="form-control" name="inputOrgName" placeholder="기관 이름" required>
                  </div>
                  <div class="col-sm-2 col-xs-2">
                    <button type="button" class="btn btn-sm btn-primary space-remove" onClick="submit_ID()" >중복</button>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputOrgHomePage" class="col-sm-3 control-label col-xs-12"><span class="red-color-text">*</span>홈페이지</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="inputOrgHomePage" value="http://" placeholder="홈페이지" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputOrgCEO" class="col-sm-3 control-label">대표자</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="inputOrgCEO" placeholder="대표자">
                  </div>
                </div>

            </div>
            <div class="col-sm-6 border">

             <h4 class="hidden-xs">　</h4>


        <!--     <article>
               <p id="status">File API & FileReader API not supported</p>
               <p><input type=file></p>
               <p>Select an image from your machine to read the contents of the file without using a server</p>
               <div id="holder"></div>
             </article>-->

            <div class="form-group">
                  <label for="inputOrgLogo" class="col-sm-12 control-label">기관 로고이미지</label>
                  <div class="col-sm-12">
                    <input type="file" class="form-control form-control-file" name="inputOrgLogo" placeholder="로고 제출">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputOrgTel" class="col-sm-4 control-label">대표전화</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="inputOrgTel" placeholder="'-' 없이 숫자만 입력해주세요">
                  </div>
                </div>

            </div>
          </div>

          <div class="row center-block">
            <div class="col-sm-6 border">
              <h4><b>관리자 정보</b></h4><span class="space"></span>

              <div class="form-group">
                <label for="inputOrgAdminName" class="col-sm-3 control-label"><span class="red-color-text">*</span>관리자명</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputOrgAdminName" placeholder="관리자명" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputOrgAdminPhone" class="col-sm-3 control-label"><span class="red-color-text">*</span>관리자  <br class="hidden-xs" />휴대전화</label>
                <div class="col-sm-9">
                  <input type="tel" class="form-control" name="inputOrgAdminPhone" placeholder="'-' 없이 숫자만 입력해주세요" required>
                </div>
              </div>

            </div>
            <div class="col-sm-6 border">

            <span class="space"></span>

              <div class="form-group">
                <label for="inputOrgAdminEmail" class="col-sm-4 control-labe col-xs-12"><span class="red-color-text">*</span>관리자 이메일</label>
                <div class="col-sm-6 col-xs-10">
                  <input type="email" class="form-control" name="inputOrgAdminEmail" placeholder="관리자 이메일" required>
                </div>
                <div class="col-sm-2 col-xs-2">
                  <button type="button" class="btn btn-sm btn-primary space-remove" onClick="submit_EMAIL()">중복</button>
                </div>
              </div>

              <div class="form-group">
                <label for="inputOrgAdminPassword" class="col-sm-4 control-label col-xs-12"><span class="red-color-text">*</span>비밀번호</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" name="inputOrgAdminPassword" placeholder="필수 입력" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputOrgAdminPassword" class="col-sm-4 control-label"><span class="red-color-text">*</span>비밀번호 확인</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" name="inputOrgAdminPassword2" placeholder="비밀번호 확인" required>
                </div>
              </div>

            </div>
          </div>
          <span class="space"></span>

          <div class="form-group text-center">
            <button type="submit" class="btn btn-primary" onClick="return check()">회원가입 <i class="fa fa-check spaceLeft"></i></button>
            <span class="space"></span>
            <button type="button" class="btn btn-warning" onClick="location.href='signIn.jsp'">가입취소 <i class="fa fa-times spaceLeft"></i></button>
          </div>
    </form>
  </div>
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap-transition.js"></script>
  <script src="js/bootstrap-alert.js"></script>
  <script src="js/bootstrap-modal.js"></script>
  <script src="js/bootstrap-dropdown.js"></script>
  <script src="js/bootstrap-scrollspy.js"></script>
  <script src="js/bootstrap-tab.js"></script>
  <script src="js/bootstrap-tooltip.js"></script>
  <script src="js/bootstrap-popover.js"></script>
  <script src="js/bootstrap-button.js"></script>
  <script src="js/bootstrap-collapse.js"></script>
  <script src="js/bootstrap-carousel.js"></script>
  <script src="js/bootstrap-typeahead.js"></script>
  <script>
  var name =0;
  var email=0;
    function submit_ID(){
       var sid = document.f2.inputOrgName.value;

       if(sid=="")
      {
        	alert("기관이름을 입력해주세요.");
        } else{
          name=1;
          window.open("duplicate_ID.jsp?inputOrgName="+sid,"","width=450 height=135");
        }
      }

      function submit_EMAIL(){
      var semail = document.f2.inputOrgAdminEmail.value;
      if(semail=="")
      {
        	alert("이메일을 입력해주세요.");
        } else{
          email=1;
           window.open("duplicate_EMAIL.jsp?inputOrgAdminEmail="+semail,"","width=450 height=135");
      }
    }

    function check(){
      if(name == 0)
      {
          alert("기관이름 중복을 확인해주세요.");
          return false;
      }else if(email ==0)
      {
          alert("관리자 이메일 중복을 확인해주세요.");
          return false;
      }
    }
    function save()
    {
      alert("여기");
    }
  </script>
  <script>
  var upload = document.getElementsByTagName('input')[0],
      holder = document.getElementById('holder'),
      state = document.getElementById('status');

  if (typeof window.FileReader === 'undefined') {
    state.className = 'fail';
  } else {
    state.className = 'success';
    state.innerHTML = 'File API & FileReader available';
  }

  upload.onchange = function (e) {
    e.preventDefault();

    var file = upload.files[0],
        reader = new FileReader();
    reader.onload = function (event) {
      var img = new Image();
      img.src = event.target.result;
      // note: no onload required since we've got the dataurl...I think! :)
      if (img.width > 560) { // holder width
        img.width = 560;
      }
      holder.innerHTML = '';
      holder.appendChild(img);
    };
    reader.readAsDataURL(file);

    return false;
  };
</script>
 </body>
</html>
