﻿<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="utf-8">
 <title>방명록 시스템 &middot; FORCS</title>
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta name="description" content="">
 <meta name="author" content="">
 <link href="css/bootstrap.css" rel="stylesheet">
 <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
 <style type="text/css">
   body {
     padding-top: 40px;
     padding-bottom: 40px;
     background-color: #f5f5f5;
   }

   .form-signin {
     max-width: 300px;
     padding: 19px 29px 29px;
     margin: 20px  auto;
     background-color: #fff;
     border: 1px solid #e5e5e5;
     -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
     border-radius: 5px;
     -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
     -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
     box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
   }

   .form-signin-heading {
     margin-bottom: 25px;
   }

   .form-signin .checkbox {
     margin-bottom: 15px;
     padding: 0  20px;
   }

   .form-signin input[type="text"],
   .form-signin input[type="password"] {
     font-size: 16px;
     height: auto;
     margin-bottom: 15px;
     padding: 7px 9px;
   }

   #btn_cen {
     text-align: center;
   }
 </style>
 <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
 <div class="container form-signin">
   <form class="" method="post" action="signIn_verify.jsp">
     <h3 class="form-signin-heading">OZ Guest Book<br/><b>Paperless System</b></h3>
     <input type="text" class="input-block-level" id="Email_address" name="Email_address" placeholder="Email address">
     <input type="password" class="input-block-level" id="Password"  name="Password" placeholder="Password">
     <label class="checkbox">
       <input type="checkbox" id="keep_login"> 이메일 저장
      </label>
     <button class="btn btn-large btn-primary btn-block" type="submit" onclick="location.href='signIn_verify.jsp'">LOGIN</button>
   </form>
   <center>
     <button class="btn btn-link" id="btn_cen" type="button" onclick="location.href='signUp.jsp'">회원가입</button>
     <button class="btn btn-link" id="btn_cen" type="button" onclick="location.href='findPassword.jsp'">비밀번호 찾기</button>
   </center>
   <center><img class="img-responsive" src="img/logo.png" alt="logo"></center>
 </div>
 <script src="js/jquery.js"></script>
 <script src="js/bootstrap-transition.js"></script>
 <script src="js/bootstrap-alert.js"></script>
 <script src="js/bootstrap-modal.js"></script>
 <script src="js/bootstrap-dropdown.js"></script>
 <script src="js/bootstrap-scrollspy.js"></script>
 <script src="js/bootstrap-tab.js"></script>
 <script src="js/bootstrap-tooltip.js"></script>
 <script src="js/bootstrap-popover.js"></script>
 <script src="js/bootstrap-button.js"></script>
 <script src="js/bootstrap-collapse.js"></script>
 <script src="js/bootstrap-carousel.js"></script>
 <script src="js/bootstrap-typeahead.js"></script>
 <script type="text/javascript">
   $(document).ready(function(){
       // 저장된 쿠키값을 가져와서 Email_address 칸에 넣어준다. 없으면 공백으로 들어감.
       var userInputEmail_address = getCookie("userInputEmail_address");
       $("input[name='Email_address']").val(userInputEmail_address);

       if($("input[name='Email_address']").val() != ""){ // 그 전에 Email_address를 저장해서 처음 페이지 로딩 시, 입력 칸에 저장된 Email_address가 표시된 상태라면,
           $("#keep_login").attr("checked", true); // Email_address 저장하기를 체크 상태로 두기.
       }

       $("#keep_login").change(function(){ // 체크박스에 변화가 있다면,
           if($("#keep_login").is(":checked")){ // Email_address 저장하기 체크했을 때,
               var userInputEmail_address = $("input[name='Email_address']").val();
               setCookie("userInputEmail_address", userInputEmail_address, 7); // 7일 동안 쿠키 보관
           }else{ // Email_address 저장하기 체크 해제 시,
               deleteCookie("userInputEmail_address");
           }
       });

       // Email_address 저장하기를 체크한 상태에서 Email_address를 입력하는 경우, 이럴 때도 쿠키 저장.
       $("input[name='Email_address']").keyup(function(){ // Email_address 입력 칸에 Email_address를 입력할 때,
           if($("#keep_login").is(":checked")){ // Email_address 저장하기를 체크한 상태라면,
               var userInputEmail_address = $("input[name='Email_address']").val();
               setCookie("userInputEmail_address", userInputEmail_address, 7); // 7일 동안 쿠키 보관
           }
       });
   });
   function setCookie(cookieName, value, exdays){
       var exdate = new Date();
       exdate.setDate(exdate.getDate() + exdays);
       var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toGMTString());
       document.cookie = cookieName + "=" + cookieValue;
   }
   function deleteCookie(cookieName){
       var expireDate = new Date();
       expireDate.setDate(expireDate.getDate() - 1);
       document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString();
   }
   function getCookie(cookieName) {
       cookieName = cookieName + '=';
       var cookieData = document.cookie;
       var start = cookieData.indexOf(cookieName);
       var cookieValue = '';
       if(start != -1){
           start += cookieName.length;
           var end = cookieData.indexOf(';', start);
           if(end == -1)end = cookieData.length;
           cookieValue = cookieData.substring(start, end);
       }
       return unescape(cookieValue);
   }
 </script>
 </body>
</html>
