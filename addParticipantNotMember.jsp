<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>
<%@ include file = "session.jsp"%>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>일반예약 &middot; FORCS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/making.css" rel="stylesheet">
  <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-ParticipantInfo {
      max-width: 900px;
      padding: 0px;
      margin: 20px auto;
      background-color: #fff;
      border: 1px solid #e5e5e5;
      -webkit-border-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

    .form-ParticipantInfo .form-ParticipantInfo-heading {
      color: white;
      padding: 50px 0;
      margin-top: 0px;
      margin-bottom: 45px;
      background-color: rgb(33, 47, 61);
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
    }

    .form-ParticipantInfo input[type="text"],
    .form-ParticipantInfo input[type="password"],
    .form-ParticipantInfo input[type="file"],
    .form-ParticipantInfo input[type="tel"],
    .form-ParticipantInfo input[type="email"],
    .form-ParticipantInfo select {
      font-size: 15px;
      height: auto;
      margin-bottom: 25px;
      padding: 7px 9px;
    }

  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
  <%
    String event_title = new String(request.getParameter("event_title").getBytes("8859_1"),"utf-8");
  %>
  <div class="container-fluid">
    <form name="addparticipantnotmember" action="addParticipantNotMember_verify.jsp?event_title=<%=event_title%>" class="form-ParticipantInfo" method = "post" enctype="multipart/form-data">

          <div class="row center-block">
            <button type="button" class="arrow-btn btn-link" onclick="location.href='manageParticipantOngoing.jsp?event_title=<%=event_title%>'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
            <h3 class="form-ParticipantInfo-heading text-center">일반 예약</h3>
          </div>

          <div class="row center-block">
            <div class="col-sm-6 border">
              <h4><b>기본 정보</b></h4><span class="space"></span>

              <div class="form-group">
                <label for="inputParticipantName" class="col-sm-3 control-label col-xs-12"><span class="red-color-text">*</span>이름</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputParticipantName" placeholder="이름" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputParticipantBelong" class="col-sm-3 control-label col-xs-12"><span class="red-color-text">*</span>소속/학교</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputParticipantBelong" placeholder="소속/학교" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputParticipantEmail" class="col-sm-3 control-label col-xs-12"><span class="red-color-text">*</span>이메일</label>
                <div class="col-sm-7 col-xs-10">
                  <input type="email" class="form-control" name="inputParticipantEmail" placeholder="이메일" required>
                </div>
                <div class="col-sm-2 col-xs-2">
                  <button type="button" class="btn btn-sm btn-primary space-remove" onclick="submit_EMAIL()">중복</button>
                  </div>
              </div>

            </div>
            <div class="col-sm-6 border">
             <h4 class="hidden-xs">　</h4><span class="space"></span>

             <div class="form-group">
               <label for="inputParticipantPhone" class="col-sm-3 control-label"><span class="red-color-text">*</span>휴대전화</label>
               <div class="col-sm-9">
                 <input type="tel" class="form-control" name="inputParticipantPhone" placeholder="'-' 없이 숫자만 입력해주세요" required>
               </div>
             </div>

             <div class="form-group">
               <label for="inputParticipantPhoto" class="col-sm-12 control-label">
                 사진
                 <div class="help-block pull-right">　(권장사이즈 : 68 * 68, png 파일)</div>
               </label>
               <div class="col-sm-12">
                 <input type="file" class="form-control form-control-file" name="inputParticipantPhoto">
               </div>
             </div>

            </div>
          </div>

          <div class="row center-block">
            <div class="col-sm-6 border">
              <h4><b>추가 정보</b></h4><span class="space"></span>

              <div class="form-group">
                <label for="inputParticipantDepartment" class="col-sm-3 control-label col-xs-12">부서/학과</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputParticipantDepartment" placeholder="부서/학과">
                </div>
              </div>

            </div>

            <div class="col-sm-6 border">
              <h4 class="hidden-xs">　</h4><span class="space"></span>

              <div class="form-group">
                <label for="inputParticipantPosition" class="col-sm-3 control-label col-xs-12">직위/학년</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputParticipantPosition" placeholder="직위/학년">
                </div>
              </div>

            </div>
          </div>
          <span class="space"></span>

          <div class="form-group text-center">
            <button type="submit" class="btn btn-primary" onclick="return checkDuplicate()">등록하기 <i class="fa fa-check spaceLeft"></i></button>
          </div>
          <span class="space"></span>
    </form>
  </div>
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap-transition.js"></script>
  <script src="js/bootstrap-alert.js"></script>
  <script src="js/bootstrap-modal.js"></script>
  <script src="js/bootstrap-dropdown.js"></script>
  <script src="js/bootstrap-scrollspy.js"></script>
  <script src="js/bootstrap-tab.js"></script>
  <script src="js/bootstrap-tooltip.js"></script>
  <script src="js/bootstrap-popover.js"></script>
  <script src="js/bootstrap-button.js"></script>
  <script src="js/bootstrap-collapse.js"></script>
  <script src="js/bootstrap-carousel.js"></script>
  <script src="js/bootstrap-typeahead.js"></script>
  <script type="text/javascript">
    var checkValue=0;

    function submit_EMAIL() {
    var email = document.addparticipantnotmember.inputParticipantEmail.value;

    if(email == "") {
        alert("이메일을 입력해주세요.");
      } else {
        window.open("duplicate_participant_email.jsp?inputParticipantEmail="+email,"","width=450 height=135");
      }
    }
    function checkDuplicate() {
      if(checkValue == 0) {
        alert("이메일 중복을 확인해 주세요.");
        return false;
      }
      else if(checkValue == 1) {
        alert("중복된 이메일입니다. 이메일을 변경해주세요.");
        return false;
      }
    }
    function ok() {
      checkValue = 2;
    }
    function no() {
      checkValue = 1;
    }
  </script>
 </body>
</html>
