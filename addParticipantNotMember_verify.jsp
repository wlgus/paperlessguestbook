<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy,jxl.*"%>
<%@ include file = "session.jsp"%>

<%
 int sizeLimit = 10 * 1024 * 1024 ;
 String savePath = request.getRealPath("");
 String temp = "img";
 String realPath = savePath+"/"+temp;
 MultipartRequest multi = new MultipartRequest(request, realPath, sizeLimit, "utf-8", new DefaultFileRenamePolicy());
 String filename = temp+"/"+multi.getFilesystemName("inputMembershipPhoto");

  String org_id = (String)session.getAttribute("id");
  String event_title = new String(request.getParameter("event_title").getBytes("8859_1"),"utf-8");
  String guest_name = multi.getParameter("inputParticipantName");
  String guest_company = multi.getParameter("inputParticipantBelong");
  String guest_email = multi.getParameter("inputParticipantEmail");
  String guest_phone = multi.getParameter("inputParticipantPhone");

  String guest_department = multi.getParameter("inputParticipantDepartment");
  String guest_position = multi.getParameter("inputParticipantPosition");
  java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
  String guest_update = formatter.format(new java.util.Date());
  Connection myConn = null;
  PreparedStatement pstmt = null;
  String mySQL = null;
  String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
  String db_user = "intern";
  String db_password = "password";
  String Driver="org.mariadb.jdbc.Driver";

  try{
      Class.forName(Driver);
      myConn = DriverManager.getConnection(db_url, db_user, db_password);
   }catch(SQLException ex){
         System.err.println("SQLException: " + ex.getMessage());
   }
  try{
    mySQL = "Insert into Guestbook(ORG_ID, TITLE, GUEST_EMAIL, GUEST_COMPANY, GUEST_DEPARTMENT, GUEST_POSITION, GUEST_NAME, GUEST_PHONE, GUEST_UPDATE, GUEST_PICTURE) values(?,?,?,?,?,?,?,?,?,?)";
    pstmt = myConn.prepareStatement(mySQL);

    pstmt.setString(1, org_id);
    pstmt.setString(2, event_title);
    pstmt.setString(3, guest_email);
    pstmt.setString(4, guest_company);
    pstmt.setString(5, guest_department);
    pstmt.setString(6, guest_position);
    pstmt.setString(7, guest_name);
    pstmt.setString(8, guest_phone);
    pstmt.setString(9, guest_update);
    pstmt.setString(10, realPath);

    if(pstmt.executeUpdate() != 0){
    %>
    <script>
      alert("일반예약 등록에 성공하였습니다.");
      location.href = "manageParticipantOngoing.jsp?event_title=<%=event_title%>";
    </script>
    <%
    }
    else{
    %>
    <script>
      alert("일반예약 등록에 실패하였습니다.");
      location.href="addParticipantNotMember.jsp?event_title=<%=event_title%>";
    </script>
    <%
    }

    } catch(Exception ex){
      ex.printStackTrace();
      System.err.println("SQLException: " + ex.getMessage());
    }finally{
    myConn.close(); pstmt.close();
    }
    %>
