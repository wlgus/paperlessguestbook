<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>
<%@ include file = "session.jsp"%>
<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <title>기관정보&middot; FORCS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/making.css" rel="stylesheet">
  <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-orgInfo {
      max-width: 900px;
      padding: 0px;
      margin: 20px auto;
      background-color: #fff;
      border: 1px solid #e5e5e5;
      -webkit-border-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

    .form-orgInfo .form-orgInfo-heading {
      color: white;
      padding: 50px 0;
      margin-top: 0px;
      margin-bottom: 45px;
      background-color: rgb(33, 47, 61);
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
    }

    .form-orgInfo input[type="text"],
    .form-orgInfo input[type="password"],
    .form-orgInfo input[type="file"],
    .form-orgInfo input[type="tel"],
    .form-orgInfo input[type="email"] {
      font-size: 15px;
      height: auto;
      margin-bottom: 25px;
      padding: 7px 9px;
    }

    .btn-location {
      font-size: 12px;
      color: rgb(111, 111, 111);
    }

  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
  <%
       Connection myConn = null;
       Statement stmt = null;
       ResultSet myResultSet;
       String mySQL;
       String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
       String db_user = "intern";
       String db_password = "password";
       String Driver = "org.mariadb.jdbc.Driver";
       String name=null;
       String email=null;
       String homepage=null;
       String image=null;
       String ceo=null;
       String tel=null;
       String admin_name=null;
       String admin_pass=null;
       String admin_phone=null;
       int id;

       try {
         Class.forName(Driver);
          myConn = DriverManager.getConnection(db_url, db_user, db_password);
          stmt = myConn.createStatement();  //include가 안되어있기때문에 해줘야한다.

       } catch (SQLException ex) {
          System.err.println("SQLException: "+ ex.getMessage());
       }

       mySQL = "select ORG_NAME, ORG_ADMIN_EMAIL, ORG_HOMEPAGE, ORG_IMAGE, ORG_CEO, ORG_TEL, ORG_ADMIN_NAME, ORG_ADMIN_PASS, ORG_ADMIN_PHONE from Organization where ORG_ID = '" + session_id +"'";
       myResultSet = stmt.executeQuery(mySQL);
       if(myResultSet.next()) {

        name = myResultSet.getString("ORG_NAME");
        email = myResultSet.getString("ORG_ADMIN_EMAIL");
        homepage = myResultSet.getString("ORG_HOMEPAGE");
        image = myResultSet.getString("ORG_IMAGE");
        ceo = myResultSet.getString("ORG_CEO");
        tel = myResultSet.getString("ORG_TEL");
        admin_name = myResultSet.getString("ORG_ADMIN_NAME");
        admin_pass = myResultSet.getString("ORG_ADMIN_PASS");
        admin_phone = myResultSet.getString("ORG_ADMIN_PHONE");
      }
  %>

  <div class="container-fluid">
    <form action="orgInfo_verify.jsp" class="form-orgInfo" name="orgInfo" method="post">
          <div class="row center-block">
            <button type="button" class="arrow-btn btn-link" onClick="location.href='main.jsp'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
            <h3 class="form-orgInfo-heading text-center">
              <span><%=name%></span> 정보
            </h3>
          </div>

          <div class="row center-block">
            <div class="col-sm-6 border">
              <h4><b>기관 정보</b></h4><span class="space"></span>

              <div class="form-group">
                  <label for="inputOrgName" class="col-sm-3 control-label col-xs-12"><span class="red-color-text">*</span>기관이름</label>
                  <div class="col-sm-7 col-xs-10">
                    <input type="text" class="form-control" name="inputOrgName" placeholder="기관 이름" value="<%=name%>">
                  </div>
                  <div class="col-sm-2 col-xs-2">
                    <button type="button" class="btn btn-sm btn-primary space-remove" onClick="submit_ID()" >중복</button>
                  </div>
                </div>

                <div class="hidden form-group">
                    <input type="text" class="hidden form-control" name="inputOriginalOrgName" placeholder="기존 기관 이름"  value="<%=name%>">
                </div>

                <div class="form-group">
                  <label for="inputOrgHomePage" class="col-sm-3 control-label col-xs-12"><span class="red-color-text">*</span>홈페이지</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="inputOrgHomePage"  value="<%=homepage%>" placeholder="홈페이지">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputOrgCEO" class="col-sm-3 control-label">대표자</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="inputOrgCEO" placeholder="대표자"  value="<%=ceo%>">
                  </div>
                </div>

            </div>
            <div class="col-sm-6 border">

             <h4>　</h4>

              <div class="form-group">
                  <label for="inputOrgLogo" class="col-sm-12 control-label">기관 로고이미지</label>
                  <div class="col-sm-12">
                    <input type="file" class="form-control form-control-file" name="inputOrgLogo" placeholder="로고 제출"  value="<%=image%>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputOrgTel" class="col-sm-4 control-label">대표전화</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="inputOrgTel" placeholder="'-' 없이 숫자만 입력해주세요" value="<%=tel%>">
                  </div>
                </div>

            </div>
          </div>

          <div class="row center-block">
            <div class="col-sm-6 border">
              <h4><b>관리자 정보</b></h4><span class="space"></span>

              <div class="form-group">
                <label for="inputOrgAdminName" class="col-sm-3 control-label"><span class="red-color-text">*</span>관리자명</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputOrgAdminName" placeholder="관리자명" value="<%=admin_name%>">
                </div>
              </div>

              <div class="form-group">
                <label for="inputOrgAdminPhone" class="col-sm-3 control-label"><span class="red-color-text">*</span>관리자 <br class="hidden-xs">휴대전화</label>
                <div class="col-sm-9">
                  <input type="tel" class="form-control" name="inputOrgAdminPhone" placeholder="'-' 없이 숫자만 입력해주세요" value="<%=admin_phone%>">
                </div>
              </div>

            </div>
            <div class="col-sm-6 border">

            <span class="space"></span>

              <div class="form-group">
                <label for="inputOrgAdminEmail" class="col-sm-4 control-labe col-xs-12"><span class="red-color-text">*</span>관리자 이메일</label>
                <div class="col-sm-6 col-xs-10">
                  <input type="email" class="form-control" name="inputOrgAdminEmail" placeholder="관리자 이메일" value="<%=email%>">
                </div>
                <div class="col-sm-2 col-xs-2">
                  <button type="button" class="btn btn-sm btn-primary space-remove" onClick="submit_EMAIL()">중복</button>
                </div>
              </div>

              <input type="email" class="hidden form-control" name="inputOriginalOrgAdminEmail" placeholder="기존 관리자 이메일" value="<%=email%>">

              <div class="form-group">
                <label for="inputOrgAdminPassword" class="col-sm-4 control-label col-xs-12"><span class="red-color-text">*</span>비밀번호</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="inputOrgAdminPassword" placeholder="비밀번호">
                </div>
              </div>

              <div class="form-group">
                <label for="inputOrgAdminPassword" class="col-sm-4 control-label"><span class="red-color-text">*</span>비밀번호 확인</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="inputOrgAdminPassword2" placeholder="비밀번호 확인">
                </div>
              </div>

            </div>
          </div>
          <span class="space"></span>

          <div class="form-group text-center">
            <button class="btn btn-primary" type="submit" onclick="return check()">수정 <i class="fa fa-check spaceLeft"></i></button>
            <span class="space"></span>
            <button type="button" class="btn btn-warning" onClick="javascript:history.back(-1)">취소 <i class="fa fa-times spaceLeft"></i></button>
          </div>
                    <button type="button" class="btn  btn-link btn-location pull-right" onClick="delete_org()">기관 탈퇴</button><br/>
          <span class="space"></span>
    </form>
  </div>
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap-transition.js"></script>
  <script src="js/bootstrap-alert.js"></script>
  <script src="js/bootstrap-modal.js"></script>
  <script src="js/bootstrap-dropdown.js"></script>
  <script src="js/bootstrap-scrollspy.js"></script>
  <script src="js/bootstrap-tab.js"></script>
  <script src="js/bootstrap-tooltip.js"></script>
  <script src="js/bootstrap-popover.js"></script>
  <script src="js/bootstrap-button.js"></script>
  <script src="js/bootstrap-collapse.js"></script>
  <script src="js/bootstrap-carousel.js"></script>
  <script src="js/bootstrap-typeahead.js"></script>

  <script type="text/javascript">
    var checkValue = 0;
    var c_id = 0, c_email = 0;
    function submit_ID(){
    var sid = document.orgInfo.inputOrgName.value;
    var original = document.orgInfo.inputOriginalOrgName.value;
     if(sid=="") {
       alert("기관이름을 입력해주세요.");
    } else{
       var url = "duplicate_ID_edit.jsp?inputOrgName="+sid+"&inputOriginalOrgName="+original;
       window.open(url,"","width=450 height=135");
     }
    }
    function submit_EMAIL(){
      var email = document.orgInfo.inputOrgAdminEmail.value;
      var original_email = document.orgInfo.inputOriginalOrgAdminEmail.value;
      if(email=="") {
         alert("이메일을 입력해주세요.");
      } else {
         var url = "duplicate_EMAIL_edit.jsp?inputOrgAdminEmail="+email+"&inputOriginalOrgAdminEmail="+original_email;
         window.open(url,"","width=450 height=135");
      }
    }
    function check(){
      if(document.orgInfo.inputOrgAdminPassword.value == "") {
          alert("비밀번호를 입력해주세요.");
          return false;
      }
      if(checkValue == 0) {
        alert("기관명과 이메일 중복 확인을 해주세요.");
        return false;
      } else  if(checkValue == 1) {
        alert("중복된 이메일 또는 기관명을 변경해주세요.");
        return false;
      } else  if(c_id != 1 || c_email != 1) {
        alert("기관명과 이메일 중복 확인을 모두 해주세요.");
        return false;
      }
    }
    function delete_org(){
        var d_org= <%=session_id%>;
        location.href="delete_org.jsp?deleteOrg="+d_org;
    }
    function ok(){
      checkValue = 2;
    }
    function ok_id(){
      c_id = 1;
    }
    function ok_email(){
      c_email = 1;
    }
    function no(){
      checkValue = 1;
    }
  </script>
 </body>
</html>
