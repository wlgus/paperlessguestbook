﻿<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>


<%
	String org_email =new String(request.getParameter("inputOrgAdminEmail").getBytes("8859_1"),"utf-8");
  String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
  String db_user = "intern";
  String db_password = "password";
  String mySQL = null;
  PreparedStatement pstmt = null;
  Connection myConn=null;
  Statement stmt=null;
  String Driver="org.mariadb.jdbc.Driver";

  try{
		Class.forName(Driver);

		myConn = DriverManager.getConnection(db_url, db_user, db_password);
	}catch(SQLException ex){
			System.err.println("SQLException: " + ex.getMessage());
	}
  try{

		mySQL = "select ORG_ADMIN_EMAIL from Organization where ORG_ADMIN_EMAIL = ?";
		pstmt = myConn.prepareStatement(mySQL);
		pstmt.setString(1, org_email);
		ResultSet myResultSet = pstmt.executeQuery();

		if(!myResultSet.next()){
%>
<script>
    alert("사용하실 수 있습니다.");
    window.close();
</script>
<%
		}
		else{
%>
<script>
		alert("중복된 이메일입니다.");
		window.close();
</script>
<%
}} catch(Exception ex){
			ex.printStackTrace();
			System.err.println("SQLException: " + ex.getMessage());
	}finally{
		myConn.close(); pstmt.close();
	}
%>
