﻿<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>

<%
	String org_admin_email =new String(request.getParameter("inputAdminEmail").getBytes("8859_1"),"utf-8");
	session.setAttribute("email", org_admin_email);
	String org_admin_phone = new String(request.getParameter("inputAdminPhone").getBytes("8859_1"),"utf-8");
	Connection myConn = null;
	PreparedStatement pstmt = null;

	String mySQL = null;

	String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
  	String db_user = "intern";
  	String db_password = "password";
	String Driver="org.mariadb.jdbc.Driver";

	try{
		Class.forName(Driver);
		myConn = DriverManager.getConnection(db_url, db_user, db_password);
	}catch(SQLException ex){
			System.err.println("SQLException: " + ex.getMessage());
	}
	try{

		mySQL = "select ORG_ADMIN_PASS from Organization where ORG_ADMIN_EMAIL = ? and ORG_ADMIN_PHONE =?";
		pstmt = myConn.prepareStatement(mySQL);
		pstmt.setString(1, org_admin_email);
		pstmt.setString(2, org_admin_phone);
		ResultSet myResultSet = pstmt.executeQuery();
		if(!myResultSet.next()){
%>
<script>
    alert("해당 정보가 존재하지 않습니다.");
    location.href="findPassword.jsp";
</script>
<%
	}
	else{
%>
<script>
	 location.href="setNewPassword.jsp";
</script>
<%
}} catch(Exception ex){
			ex.printStackTrace();
			System.err.println("SQLException: " + ex.getMessage());
	}finally{
		myConn.close(); pstmt.close();
	}
%>
