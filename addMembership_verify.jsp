﻿<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy,jxl.*"%>

<%
  int sizeLimit = 10 * 1024 * 1024 ;
  String savePath = request.getRealPath("");
  String temp = "img";
  String realPath = savePath+"/"+temp;
  MultipartRequest multi = new MultipartRequest(request, realPath, sizeLimit, "utf-8", new DefaultFileRenamePolicy());
  String filename = temp+"/"+multi.getFilesystemName("inputMembershipPhoto");

  String org_id = (String)session.getAttribute("id");
	String membership_name =multi.getParameter("inputMembershipName");
	String membership_type = multi.getParameter("inputMembershipDivision");
	String membership_main_item = multi.getParameter("inputMembershipMainItem");
	String membership_level = multi.getParameter("inputMembershipLevel");
	String membership_homepage = multi.getParameter("inputMembershipHomePage");

	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
 	String membership_update = formatter.format(new java.util.Date());
	Connection myConn = null;
	PreparedStatement pstmt = null;
  Statement stmt=null;
	String mySQL = null;
	String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
	String db_user = "intern";
	String db_password = "password";
	String Driver="org.mariadb.jdbc.Driver";
  ResultSet myResultSet;

	try {
		Class.forName(Driver);
		myConn = DriverManager.getConnection(db_url, db_user, db_password);
	} catch (SQLException ex){
			System.err.println("SQLException: " + ex.getMessage());
	}

	try{
		mySQL ="Insert into Membership(ORG_ID, MEMBERSHIP_NAME, MEMBERSHIP_LEVEL, HOMEPAGE, LOGO_IMAGE, TYPE, MAIN_ITEM, UPDATE_DATE) values(?,?,?,?,?,?,?,?)";

		pstmt = myConn.prepareStatement(mySQL);

		pstmt.setString(1, org_id);
		pstmt.setString(2, membership_name);
		pstmt.setString(3, membership_level);
		pstmt.setString(4, membership_homepage);
		pstmt.setString(5, filename);
		pstmt.setString(6, membership_type);
		pstmt.setString(7, membership_main_item);
		pstmt.setString(8, membership_update);

		if(pstmt.executeUpdate() != 0){
%>
<script>
			alert("회원사 등록이 완료되었습니다.");
			location.href="manageMembership.jsp";
</script>
<%
		}
		else{
 %>
  <script>
  			alert("회원사등록이 불가능합니다.");
  			location.href="addMembership.jsp";
  </script>
 <%
		}
	} catch(Exception ex){
			ex.printStackTrace();
			System.err.println("SQLException: " + ex.getMessage());
	} finally {
		myConn.close(); pstmt.close();
	}
%>
