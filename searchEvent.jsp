<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>
<%@ include file = "session.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>행사검색 &middot; FORCS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/making.css" rel="stylesheet">
  <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-searchEvent {
      max-width: 900px;
      padding: 0px;
      margin: 20px auto;
      background-color: #fff;
      border: 1px solid #e5e5e5;
      -webkit-border-top-right-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

    .form-searchEvent .form-searchEvent-heading {
      color: white;
      padding: 50px 0;
      margin-top: 0px;
      margin-bottom: 35px;
      background-color: rgb(33, 47, 61);
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
    }

    .form-searchEvent input[type="text"],
    .form-searchEvent input[type="date"]{
      font-size: 13px;
      height: 38px;
      margin-bottom: 15px;
    }
  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
  <div class="container-fluid">
    <form action="searchEvent_result.jsp" class="form-searchEvent " method="post">

          <div class="row center-block">
            <button type="button" class="arrow-btn btn-link" onclick="location.href='manageEventOngoing.jsp'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
            <h3 class="form-searchEvent-heading text-center">
              행사 검색
            </h3>
          </div>

          <div class="row center-block">
            <div class="col-sm-12 border">
              <h4><b>전체 일정</b></h4><span class="space"></span>

              <form class="form-searchEvent" method="post" action="searchEvent_result.jsp">
                <div class="form-group col-sm-12">
                  <label for="inputEventName" class="col-sm-2 control-label col-xs-12">행사명</label>
                  <div class="col-sm-8 col-xs-8">
                    <input type="text" class="form-control" name="inputEventName" placeholder="행사명">
                  </div>
                  <div class="col-sm-2 col-xs-4">
                    <button type="submit" class="btn btn-sm btn-primary space-remove btn-block" >검색</button>
                  </div>
                </div>

                <span class="space"></span>

                <div class="form-group col-sm-12">
                  <label for="inputEventDate" class="col-sm-2 control-label col-xs-12">기간선택</label>
                  <div class="col-sm-3">
                    <input type="date" class="form-control" id="inputEventDate1" name="inputEventDate1" value="">
                  </div>

                  <div class="col-sm-3">
                    <input type="date" class="form-control" id="inputEventDate2" name="inputEventDate2">
                  </div>
                  <div class="col-sm-4">
                    <div class="btn-group" role="group" aria-label="">
                      <button type="button" onclick="addMonth(1)" class="btn btn-sm btn-primary">1개월</button>
                      <button type="button" onclick="addMonth(3)"  class="btn btn-sm btn-primary">3개월</button>
                      <button type="button"  onclick="addMonth(6)"  class="btn btn-sm btn-primary">6개월</button>
                    </div>
                  </div>
              </form>

              </div>

              <span class="space"></span>


              <br>
              <div class="table-responsive">
                <table class="table table-hover table-bordered">
                  <thead>
                    <tr>
                      <th class="text-center"><b>행사명</b></th>
                      <th class="text-center"><b>행사 기간</b></b></th>
                      <th class="text-center"><b>장소</b></th>
                      <th class="text-center"><b>선택</b></th>
                      <th class="text-center"><b>안내</b></th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
              <span class="space"></span>
            </div>
            </div>
          </div>

    </form>
  </div>
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap-transition.js"></script>
  <script src="js/bootstrap-alert.js"></script>
  <script src="js/bootstrap-modal.js"></script>
  <script src="js/bootstrap-dropdown.js"></script>
  <script src="js/bootstrap-scrollspy.js"></script>
  <script src="js/bootstrap-tab.js"></script>
  <script src="js/bootstrap-tooltip.js"></script>
  <script src="js/bootstrap-popover.js"></script>
  <script src="js/bootstrap-button.js"></script>
  <script src="js/bootstrap-collapse.js"></script>
  <script src="js/bootstrap-carousel.js"></script>
  <script src="js/bootstrap-typeahead.js"></script>
  <script>
    document.getElementsByName('inputEventDate1')[0].valueAsDate = new Date();
  </script>
  <script>
    function addMonth(pMonth)
    {
      var cDate, oDate;
      var cYear, cMonth, cDay;
      cDate = new Date();
      cYear = cDate.getFullYear();
      cMonth = (cDate.getMonth()*1)+ ((pMonth*1));
      cDay = cDate.getDate();
      oDate = new Date(cYear, cMonth, cDay);
      document.getElementsByName('inputEventDate2')[0].valueAsDate = oDate;
      return;
      }
    </script>
 </body>
</html>
