﻿<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>
<%@ include file = "session.jsp"%>

<%
String member_original_name =new String(request.getParameter("inputOriginalName").getBytes("8859_1"),"utf-8");
String member_name =new String(request.getParameter("inputMemberName").getBytes("8859_1"),"utf-8");
String member_email = new String(request.getParameter("inputMemberEmail").getBytes("8859_1"),"utf-8");
String member_phone = new String(request.getParameter("inputMemberPhone").getBytes("8859_1"),"utf-8");
String member_department = new String(request.getParameter("inputMemberDepartment").getBytes("8859_1"),"utf-8");
String member_position = new String(request.getParameter("inputMemberPosition").getBytes("8859_1"),"utf-8");
String member_photo = new String(request.getParameter("inputMemberPhoto").getBytes("8859_1"),"utf-8");
String member_tel = new String(request.getParameter("inputMemberTel").getBytes("8859_1"),"utf-8");
java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
String member_update = formatter.format(new java.util.Date());
Connection myConn = null;
PreparedStatement pstmt = null;
String mySQL = null;
String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
String db_user = "intern";
String db_password = "password";
String Driver="org.mariadb.jdbc.Driver";

try {
     Class.forName(Driver);
     myConn = DriverManager.getConnection(db_url, db_user, db_password);
}catch(SQLException ex) {
         System.err.println("SQLException: " + ex.getMessage());
}

try{
   mySQL = "Update Member set MEM_NAME=?, MEM_EMAIL=?, MEM_PHONE=?, MEM_DEPARTMENT=?, MEM_POSITION=?, MEM_PICTURE=?, MEM_TEL=?, MEM_UPDATE=? where ORG_ID = '" + session_id +"' and MEM_NAME = '"+member_original_name+"'";

     pstmt = myConn.prepareStatement(mySQL);

     pstmt.setString(1, member_name);
     pstmt.setString(2, member_email);
     pstmt.setString(3, member_phone);
     pstmt.setString(4, member_department);
     pstmt.setString(5, member_position);
     pstmt.setString(6, member_photo);
     pstmt.setString(7, member_tel);
     pstmt.setString(8, member_update);

     if(pstmt.executeUpdate() != 0){
%>
<script>
        alert("정보 수정을 완료하였습니다.");
        location.href = "manageMember.jsp";
</script>
<%
     }
     else{
%>
<script>
        alert("정보 수정에 실패하였습니다.");
        location.href="manageMember.jsp";
</script>
<%
     }

  } catch(Exception ex){
        ex.printStackTrace();
        System.err.println("SQLException: " + ex.getMessage());
  }finally{
     myConn.close(); pstmt.close();
  }
%>
