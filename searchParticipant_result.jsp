<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>
<%@ include file = "session.jsp"%>

<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="utf-8">
 <title>참석자검색 &middot; FORCS</title>
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta name="description" content="">
 <meta name="author" content="">
 <link href="css/bootstrap.css" rel="stylesheet">
 <link href="css/making.css" rel="stylesheet">
 <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
 <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
 <style type="text/css">
   body {
     padding-top: 40px;
     padding-bottom: 40px;
     background-color: #f5f5f5;
   }

   .form-searchParticipant {
     max-width: 900px;
     padding: 0px;
     margin: 20px auto;
     background-color: #fff;
     border: 1px solid #e5e5e5;
     -webkit-border-top-right-radius: 5px;
     -moz-border-radius: 5px;
     border-radius: 5px;
     -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
     -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
     box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
   }

   .form-searchParticipant .form-searchParticipant-heading {
     color: white;
     padding: 50px 0;
     margin-top: 0px;
     margin-bottom: 35px;
     background-color: rgb(33, 47, 61);
     border-top-right-radius: 5px;
     border-top-left-radius: 5px;
   }

   .form-searchParticipant input[type="text"],
   .form-searchParticipant input[type="password"],
   .form-searchParticipant input[type="file"],
   .form-searchParticipant input[type="tel"],
   .form-searchParticipant input[type="email"],
   .form-searchParticipant input[type="number"],
   .form-searchParticipant select {
     font-size: 14px;
     height: auto;
     margin-bottom: 10px;
     padding: 7px 9px;
   }

 </style>
 <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
 <%
   String event_title = new String(request.getParameter("event_title").getBytes("8859_1"),"utf-8");
 %>
 <div class="container-fluid">
   <form name="searchParticipant" action="searchParticipant_result.jsp?event_title=<%=event_title%>" class="form-searchParticipant" method="post">

         <div class="row center-block">
           <button type="button" class="arrow-btn btn-link" onclick="location.href='manageParticipant.jsp?event_title=<%=event_title%>'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
           <h3 class="form-searchParticipant-heading text-center">
             참석자 검색
           </h3>
         </div>

         <div class="row center-block">
           <div class="col-sm-12 border">
             <span class="space"></span>

             <form class="form-searchParticipant">
               <div class="form-group col-sm-12">
                 <label for="inputParticipantName" class="col-sm-2"><span class="red-color-text"></span>이름</label>
                 <div class="col-sm-8">
                   <input type="text" class="form-control" name="inputParticipantName" placeholder="이름">
                 </div>
               </div>

               <div class="form-group col-sm-12">
                 <label for="inputParticipantCompany" class="col-sm-2">소속</label>
                 <div class="col-sm-8">
                   <input type="text" class="form-control" name="inputParticipantCompany" placeholder="소속">
                 </div>
               </div>

               <div class="form-group col-sm-12">
                 <label for="inputParticipantPhone" class="col-sm-2">휴대전화</label>
                 <div class="col-sm-8">
                   <input type="text" class="form-control" name="inputParticipantPhone" placeholder="휴대전화">
                 </div>
                 <br class="visible-xs" />
                 <div class="col-sm-2">
                   <button type="submit" class="btn btn-sm btn-primary btn-block" onclick="return searchCheck()">검색</button>
                 </div>
               </div>

             </form>

             <span class="space"></span>

             <div class="" align="right">
               <button type="button" class="btn btn-link btn-sm" onClick="return delete_participant()">참석 취소 <i class="fa fa-minus-circle" aria-hidden="true"></i></button>
             </div>
             <br>
             <div class="table-responsive">
               <table class="table table-hover table-bordered">
                 <thead>
                   <tr>
                     <th class="text-center"><b>No.</b></th>
                     <th class="text-center"><b>소속</b></th>
                     <th class="text-center"><b>이름</b></th>
                     <th class="text-center"><b>직위</b></th>
                     <th class="text-center"><b>연락처</b></th>
                     <th class="text-center"><b>이메일</b></th>
                     <th class="text-center"><b>참석 취소</b></th>
                   </tr>
                 </thead>
                 <%
                                    String participant_name =new String(request.getParameter("inputParticipantName").getBytes("8859_1"),"utf-8");
                                    String participant_company =new String(request.getParameter("inputParticipantCompany").getBytes("8859_1"),"utf-8");
                                    String participant_phone =new String(request.getParameter("inputParticipantPhone").getBytes("8859_1"),"utf-8");

                                    Connection myConn = null;
                                    ResultSet myResultSet;
                                    ResultSet myResultSet2;
                                    Statement stmt = null;
                                    PreparedStatement pstmt = null;

                                    String mySQL = null;
                                    String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
                                    String db_user = "intern";
                                    String db_password = "password";
                                    String Driver="org.mariadb.jdbc.Driver";

                                    int num=0;
                                    String company="";
                                    String name="";
                                    String position="";
                                    String phone ="";
                                    String email="";

                                    try {
                                      Class.forName(Driver);
                                      myConn = DriverManager.getConnection(db_url, db_user, db_password);
                                    } catch(SQLException ex) {
                                        System.err.println("SQLException: " + ex.getMessage());
                                      }
                                    try{
                                      mySQL = "select GUEST_COMPANY, GUEST_NAME, GUEST_POSITION, GUEST_PHONE, GUEST_EMAIL from Guestbook where ORG_ID = '"+ session_id +"' and TITLE = '"+ event_title +"'";
                                      stmt=myConn.createStatement();
                                      myResultSet=stmt.executeQuery(mySQL);

                                      while(myResultSet.next()){
                                        mySQL="select TITLE, GUEST_EMAIL from Guestbook where GUEST_NAME like ? and GUEST_COMPANY like ? and GUEST_PHONE like ?";
                                        pstmt=myConn.prepareStatement(mySQL);
                                        pstmt.setString(1, "%" + participant_name + "%");
                                        pstmt.setString(2, "%" + participant_company + "%");
                                        pstmt.setString(3, "%" + participant_phone + "%");

                                        myResultSet2 = pstmt.executeQuery();
                                        while(myResultSet2.next()){
                                          if(myResultSet.getString("GUEST_EMAIL").equals(myResultSet2.getString("GUEST_EMAIL"))){
                                            if(myResultSet2.getString("TITLE").equals(event_title)){
                                              num++;
                                              company = myResultSet.getString("GUEST_COMPANY");
                                              name = myResultSet.getString("GUEST_NAME");
                                              position = myResultSet.getString("GUEST_POSITION");
                                              phone = myResultSet.getString("GUEST_PHONE");
                                              email = myResultSet.getString("GUEST_EMAIL");

                                              %>
                                              <tbody>
                                                <tr>
                                                  <td class="text-center"><%=num%></td>
                                                  <td class="text-center"><%=company%></td>
                                                  <td class="text-center"><%=name%></td>
                                                  <td class="text-center"><%=position%></td>
                                                  <td class="text-center"><%=phone%></td>
                                                  <td class="text-center"><%=email%></td>
                                                  <td class="text-center"><input type="checkbox" name="cancel" value="<%=email%>"></td>
                                                </tr>
                                              </tbody>
                                              <%
                                            }
                                          }
                                        }
                                    }
                                  } catch(Exception ex){
                                        ex.printStackTrace();
                                        System.err.println("SQLException: " + ex.getMessage());
                                  } finally{
                                     myConn.close(); stmt.close();
                                  }
                                  %>
               </table>
               <div class="text-center">
                 <ul class="pagination">
                   <li><a href="#">1</a></li>
                   <li><a href="#">2</a></li>
                   <li><a href="#">3</a></li>
                   <li><a href="#">4</a></li>
                   <li><a href="#">5</a></li>
                 </ul>
               </div>
             </div>
           </div>
           </div>
         </div>

   </form>
 </div>
 <script src="js/jquery.js"></script>
 <script src="js/bootstrap-transition.js"></script>
 <script src="js/bootstrap-alert.js"></script>
 <script src="js/bootstrap-modal.js"></script>
 <script src="js/bootstrap-dropdown.js"></script>
 <script src="js/bootstrap-scrollspy.js"></script>
 <script src="js/bootstrap-tab.js"></script>
 <script src="js/bootstrap-tooltip.js"></script>
 <script src="js/bootstrap-popover.js"></script>
 <script src="js/bootstrap-button.js"></script>
 <script src="js/bootstrap-collapse.js"></script>
 <script src="js/bootstrap-carousel.js"></script>
 <script src="js/bootstrap-typeahead.js"></script>
 <script LANGUAGE="JavaScript">
function searchCheck(){
 if(document.searchParticipant.inputParticipantName.value=="" && document.searchParticipant.inputParticipantCompany.value=="" && document.searchParticipant.inputParticipantPhone.value=="" ){
   alert("검색할 항목을 입력해 주세요.");
   return false;
 }
 else{
   return true;
 }
}

function delete_participant(){
 var temp=document.getElementsByName("cancel");
 var length = temp.length;
 var flag=0;

 for(var i=0;i<length;i++){
   if(temp[i].checked){
     flag=1;
   }
 }

 if(temp[0]==undefined){
   alert("삭제할 참석자가 없습니다.");
   return false;
 }
 else if(flag==0){
   alert("삭제하고 싶은 참석자를 선택해 주세요.");
   return false;
 }
 else{
   document.searchParticipant.action="delete_participant.jsp?event_title=<%=event_title%>";
   document.searchParticipant.submit();
   return true;
 }
}

   </script>
</body>
</html>
