<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ include file = "session.jsp"%>

<%
String org_id = (String)session.getAttribute("id");
String event_title = new String(request.getParameter("event_title").getBytes("8859_1"),"utf-8");
String[] participant_email = request.getParameterValues("addParticipant");

java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
String guest_update = formatter.format(new java.util.Date());

String guest_company = null;
String guest_department = null;
String guest_name = null;
String guest_position = null;
String guest_phone =  null;
String guest_picture = null;
String guest_email = null;
String mySQL = null;
String mySQL2 = null;
ResultSet myResultSet;
Connection myConn=null;
Statement stmt=null;
PreparedStatement pstmt = null;
String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
String db_user = "intern";
String db_password = "password";
String Driver="org.mariadb.jdbc.Driver";

try{
Class.forName(Driver);
myConn = DriverManager.getConnection(db_url, db_user, db_password);
stmt = myConn.createStatement();
}catch(SQLException ex){
System.err.println("SQLException: " + ex.getMessage());
}

try{

for(int i=0; i<participant_email.length; i++){
mySQL = "select MEMBERSHIP_NAME, MEM_DEPARTMENT, MEM_NAME, MEM_POSITION, MEM_PHONE, MEM_EMAIL, MEM_PICTURE from Member where MEM_EMAIL = '"+ participant_email[i] + "'";
myResultSet = stmt.executeQuery(mySQL);

if(myResultSet.next()){
guest_company =  myResultSet.getString("MEMBERSHIP_NAME");
guest_department = myResultSet.getString("MEM_DEPARTMENT");
guest_name = myResultSet.getString("MEM_NAME");
guest_position = myResultSet.getString("MEM_POSITION");
guest_phone =  myResultSet.getString("MEM_PHONE");
guest_picture = myResultSet.getString("MEM_PICTURE");
guest_email = myResultSet.getString("MEM_EMAIL");

mySQL2 = "Insert into Guestbook(ORG_ID, TITLE, GUEST_EMAIL, GUEST_COMPANY, GUEST_DEPARTMENT, GUEST_POSITION, GUEST_NAME, GUEST_PHONE, GUEST_UPDATE, GUEST_PICTURE) values(?,?,?,?,?,?,?,?,?,?)";

pstmt = myConn.prepareStatement(mySQL2);

pstmt.setString(1, org_id);
pstmt.setString(2, event_title);
pstmt.setString(3, guest_email);
pstmt.setString(4, guest_company);
pstmt.setString(5, guest_department);
pstmt.setString(6, guest_position);
pstmt.setString(7, guest_name);
pstmt.setString(8, guest_phone);
pstmt.setString(9, guest_update);
pstmt.setString(10, guest_picture);
pstmt.executeUpdate();
pstmt.close();
}
}

%>
<script>
alert("회원예약 등록에 성공하였습니다.");
location.href = "manageParticipantOngoing.jsp?event_title=<%=event_title%>";
</script>
<%
}catch(Exception ex){
ex.printStackTrace();
System.err.println("SQLException: " + ex.getMessage());
}finally{
myConn.close();  stmt.close();
}
%>
