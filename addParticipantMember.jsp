<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>
<%@ include file = "session.jsp"%>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>회원예약 &middot; FORCS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/making.css" rel="stylesheet">
  <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-ParticipantInfo {
      max-width: 900px;
      padding: 0px;
      margin: 20px auto;
      background-color: #fff;
      border: 1px solid #e5e5e5;
      -webkit-border-top-right-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

    .form-ParticipantInfo .form-ParticipantInfo-heading {
      color: white;
      padding: 50px 0;
      margin-top: 0px;
      margin-bottom: 35px;

      background-color: rgb(33, 47, 61);
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
    }

    .no-bold {
      font-weight: normal;
    }

  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
  <%
    String event_title = new String(request.getParameter("event_title").getBytes("8859_1"),"utf-8");
  %>

  <div class="container-fluid">
    <form name="addParticipantMember" class="form-ParticipantInfo" method = "post">

          <div class="row center-block">
            <button type="button" class="arrow-btn btn-link" onclick="location.href='manageParticipant.jsp?event_title=<%=event_title%>'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
            <h3 class="form-ParticipantInfo-heading text-center">
              회원 예약
            </h3>
          </div>

          <div class="row center-block">
            <div class="col-sm-12 border">
              <h4><b>회원 예약</b></h4><span class="space"></span>

                  <form class="form-searchParticipant">
                    <div class="form-group col-sm-12">
                      <label for="searchParticipantName" class="col-sm-2"><span class="red-color-text"></span>이름</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="searchParticipantName" placeholder="이름">
                      </div>
                    </div>

                    <div class="form-group col-sm-12">
                      <label for="searchParticipantCompany" class="col-sm-2">소속</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="searchParticipantCompany" placeholder="소속">
                      </div>
                      <br class="visible-xs" />
                      <div class="col-sm-2">
                        <button type="submit" class="btn btn-sm btn-primary btn-block" onclick="return searchCheck(1)">검색</button>
                      </div>
                    </div>
                  </form>
                </div>

                <span class="space"></span>
              </form>

              <div class="" align="right">
                <button type="submit" class="btn btn-link btn-sm" onclick="return searchCheck(2)">저장 <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
              </div>
              <br>

              <div class="table-responsive">
                <table class="table table-hover table-bordered">
                  <thead>
                    <tr>
                      <th class="text-center"><b>No.</b></th>
                      <th class="text-center"><b>소속</b></th>
                      <th class="text-center"><b>부서</b></th>
                      <th class="text-center"><b>이름</b></th>
                      <th class="text-center"><b>직위</b></th>
                      <th class="text-center"><b>연락처</b></th>
                      <th class="text-center"><b>이메일</b></th>
                      <th class="text-center"><b>참석</b></th>
                    </tr>
                  </thead>
                  <%

                    Connection myConn = null;
                    Statement stmt = null;
                    PreparedStatement pstmt = null;
                    ResultSet rs;
                    ResultSet rs2;
                    ResultSet paging;
                    String mySQL = null;
                    String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
                    String db_user = "intern";
                    String db_password = "password";
                    String Driver="org.mariadb.jdbc.Driver";
                    int number =0;

                    try{
                        Class.forName(Driver);
                        myConn = DriverManager.getConnection(db_url, db_user, db_password);
                        stmt = myConn.createStatement();
                      }catch(SQLException ex){
                        System.err.println("SQLException: " + ex.getMessage());
                      }

                      mySQL ="select m.MEMBERSHIP_NAME, m.MEM_DEPARTMENT, m.MEM_NAME, m.MEM_POSITION, m.MEM_PHONE, m.MEM_EMAIL from Member m where ORG_ID = '"+ session_id +"' AND NOT EXISTS (   Select * from Guestbook g where g.TITLE = '"+event_title+"' AND g.GUEST_EMAIL = m.MEM_EMAIL )";

                      rs = stmt.executeQuery(mySQL);
                      if(rs != null){
                        while(rs.next()){
                          number++;
                          String membership_name = rs.getString("MEMBERSHIP_NAME");
                          String mem_department = rs.getString("MEM_DEPARTMENT");
                          String mem_name = rs.getString("MEM_NAME");
                          String mem_position = rs.getString("MEM_POSITION");
                          String mem_phone = rs.getString("MEM_PHONE");
                          String mem_email = rs.getString("MEM_EMAIL");

                  %>
                  <tbody class="table-font-size">
                    <tr>
                      <td class="text-center"><%=number%></td>
                      <td class="text-center"><%=membership_name%></td>
                      <td class="text-center"><%=mem_department%></td>
                      <td class="text-center"><%=mem_name%></td>
                      <td class="text-center"><%=mem_position%></td>
                      <td class="text-center"><%=mem_phone%></td>
                      <td class="text-center"><%=mem_email%></td>
                      <td class="text-center"><input type="checkbox" name = "addParticipant" value = "<%=mem_email%>"></td>
                    </tr>
                  </tbody>
                  <%
                  }
                }
                %>
                </table>
              </div>
            </div>
            </div>
          </div>

    </form>
  </div>
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap-transition.js"></script>
  <script src="js/bootstrap-alert.js"></script>
  <script src="js/bootstrap-modal.js"></script>
  <script src="js/bootstrap-dropdown.js"></script>
  <script src="js/bootstrap-scrollspy.js"></script>
  <script src="js/bootstrap-tab.js"></script>
  <script src="js/bootstrap-tooltip.js"></script>
  <script src="js/bootstrap-popover.js"></script>
  <script src="js/bootstrap-button.js"></script>
  <script src="js/bootstrap-collapse.js"></script>
  <script src="js/bootstrap-carousel.js"></script>
  <script src="js/bootstrap-typeahead.js"></script>
  <script>
  function searchCheck(num){
    if(num=="1"){
      if(document.addParticipantMember.searchParticipantName.value=="" && document.addParticipantMember.searchParticipantCompany.value==""){
        alert("검색할 항목을 입력해 주세요.");
        return false;
      }
      else{
        document.addParticipantMember.action="addParticipantMember_result.jsp?event_title=<%=event_title%>";
        return true;
      }
    }
    else{
      var check_btn = document.getElementsByName("addParticipant");
      var check_btn_check = 0;
      for(var i = 0; i<check_btn.length; i++){
        if(check_btn[i].checked==true){
          check_btn_check++;
        }
      }
      if(check_btn_check==0)
      {
        alert("예약할 회원을 선택하세요.");
        return false;
      }
      else
      {
        document.addParticipantMember.action="addParticipantMember_verify.jsp?event_title=<%=event_title%>";
      }
    }
    document.addParticipantMember.submit();
}
  </script>
 </body>
</html>
