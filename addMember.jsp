<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>
<%@ include file = "session.jsp"%>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>회원등록 &middot; FORCS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/making.css" rel="stylesheet">
  <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-memberInfo {
      max-width: 900px;
      padding: 0px;
      margin: 20px auto;
      background-color: #fff;
      border: 1px solid #e5e5e5;
      -webkit-border-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

    .form-memberInfo .form-memberInfo-heading {
      color: white;
      padding: 50px 0;
      margin-top: 0px;
      margin-bottom: 45px;
      background-color: rgb(33, 47, 61);
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
    }

    .form-memberInfo input[type="text"],
    .form-memberInfo input[type="password"],
    .form-memberInfo input[type="file"],
    .form-memberInfo input[type="tel"],
    .form-memberInfo input[type="email"],
    .form-memberInfo select {
      font-size: 15px;
      height: auto;
      margin-bottom: 25px;
      padding: 7px 9px;
    }

  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
  <div class="container-fluid">
    <form name="addMember" action="addMember_verify.jsp" class="form-memberInfo"  method= "post" enctype="multipart/form-data">

          <div class="row center-block">
            <button type="button" class="arrow-btn btn-link" onClick="location.href='manageMember.jsp'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
            <h3 class="form-memberInfo-heading text-center">회원 등록</h3>
          </div>

          <div class="row center-block">
            <div class="col-sm-6 border">
              <h4><b>회사 정보</b></h4><span class="space"></span>

              <div class="form-group">
                <label for="inputMembershipName" class="col-sm-3 control-label col-xs-12"><span class="red-color-text">*</span>회사명</label>
                <div class="col-sm-9">
                  <select id="selectBox" class="form-control" name="inputMembershipName" onchange="selectMembershipName()" required>
                    <option value="0" selected>선택하세요</option>
                    <%
                        Connection myConn = null;
                        Statement stmt = null;
                        ResultSet myResultSet;
                        Connection myConn2 = null;
                        Statement stmt2 = null;
                        ResultSet myResultSet2;
                        Connection myConn3 = null;
                        Statement stmt3 = null;
                        ResultSet myResultSet3;

                        String mySQL = null;
                        String mySQL2 = null;
                        String mySQL3 = null;
                        String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
                        String db_user = "intern";
                        String db_password = "password";
                        String Driver="org.mariadb.jdbc.Driver";

                        String smembership_level="";
                        String smain_item="";
                        String shomepage="";
                        String stype="";

                        int i=0;
                        int count=0;
                        int arrayLength=0;

                        try{
                          Class.forName(Driver);
                          myConn3 = DriverManager.getConnection(db_url, db_user, db_password);

                        }catch(SQLException ex){
                          System.err.println("SQLException: " + ex.getMessage());
                        }
                        mySQL3 = "select MEMBERSHIP_NAME from Membership where Membership.ORG_ID = '"+ session_id +"'";
                        stmt3=myConn3.createStatement();
                        myResultSet3=stmt3.executeQuery(mySQL3);

                        if(myResultSet3 != null) {
                          while(myResultSet3.next()){
                            arrayLength++;
                          }
                        }

                        String temp[][]=new String[arrayLength][5];

                        try{
                          Class.forName(Driver);
                          myConn = DriverManager.getConnection(db_url, db_user, db_password);

                        }catch(SQLException ex){
                          System.err.println("SQLException: " + ex.getMessage());
                        }
                        try{
                          Class.forName(Driver);
                          myConn2 = DriverManager.getConnection(db_url, db_user, db_password);

                        }catch(SQLException ex){
                          System.err.println("SQLException: " + ex.getMessage());
                        }
                        mySQL = "select MEMBERSHIP_NAME from Membership where Membership.ORG_ID = '"+ session_id +"'";
                        stmt=myConn.createStatement();
                        myResultSet=stmt.executeQuery(mySQL);

                        if(myResultSet != null) {
                          while(myResultSet.next()){
                            String membership_name = myResultSet.getString("MEMBERSHIP_NAME");
                    %>
                            <option value="<%=membership_name%>"><%=membership_name%></option>
                    <%
                            temp[i][0]=membership_name;

                            mySQL2="select MEMBERSHIP_LEVEL, MAIN_ITEM, TYPE, HOMEPAGE from Membership where MEMBERSHIP_NAME = '"+ membership_name +"'";
                            stmt2=myConn2.createStatement();
                            myResultSet2=stmt2.executeQuery(mySQL2);

                            if(myResultSet2 != null){
                              while(myResultSet2.next()){
                                temp[i][1] = myResultSet2.getString("MEMBERSHIP_LEVEL");
                                temp[i][2] = myResultSet2.getString("MAIN_ITEM");
                                temp[i][3] = myResultSet2.getString("TYPE");
                                temp[i][4] = myResultSet2.getString("HOMEPAGE");
                              }
                            }
                            myResultSet2.close();
                            stmt2.close();
                            i++;
                          }
                        }
                        myResultSet.close();
                        stmt.close();
                    %>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="inputMembershipLevel" class="col-sm-3 control-label col-xs-12">회원사레벨</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputMembershipLevel" value="" disabled>
                </div>
              </div>

              <div class="form-group">
                <label for="inputMembershipMainItem" class="col-sm-3 control-label col-xs-12">주력 품목</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputMembershipMainItem" value="" disabled>
                </div>
              </div>

            </div>
            <div class="col-sm-6 border">
             <h4 class="hidden-xs">　</h4><span class="space"></span>

             <div class="form-group">
               <label for="inputMembershipDivision" class="col-sm-3 control-label col-xs-12">구분</label>
               <div class="col-sm-9">
                 <input type="text" class="form-control" name="inputMembershipDivision" value="" disabled>
               </div>
             </div>

             <div class="form-group">
               <label for="inputMembershipHomePage" class="col-sm-3 control-label col-xs-12">홈페이지</label>
               <div class="col-sm-9">
                 <input type="text" class="form-control" name="inputMembershipHomePage" value="" disabled>
               </div>
             </div>

            </div>
          </div>

          <div class="row center-block">
            <div class="col-sm-6 border">
              <h4><b>회원 정보</b></h4><span class="space"></span>

              <div class="form-group">
                <label for="inputMemberName" class="col-sm-3 control-label"><span class="red-color-text">*</span>이름</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputMemberName" placeholder="이름" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputMemberEmail" class="col-sm-3 control-labe col-xs-12"><span class="red-color-text">*</span>이메일</label>
                <div class="col-sm-7 col-xs-10">
                  <input type="email" class="form-control" name="inputMemberEmail" placeholder="이메일" required>
                </div>
                <div class="col-sm-2 col-xs-2">
                  <button type="button" class="btn btn-sm btn-primary space-remove" onClick="submit_EMAIL()">중복</button>
                </div>
              </div>

              <div class="form-group">
                <label for="inputMemberPhone" class="col-sm-3 control-label"><span class="red-color-text">*</span>휴대전화</label>
                <div class="col-sm-9">
                  <input type="tel" class="form-control" name="inputMemberPhone" placeholder="'-' 없이 숫자만 입력해주세요" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputMemberTel" class="col-sm-3 control-label">회사전화</label>
                <div class="col-sm-9">
                  <input type="tel" class="form-control" name="inputMemberTel" placeholder="'-' 없이 숫자만 입력해주세요">
                </div>
              </div>

            </div>
            <div class="col-sm-6 border">
              <h4 class="hidden-xs">　</h4><span class="space"></span>

              <div class="form-group">
                <label for="inputMemberPosition" class="col-sm-3 control-label"><span class="red-color-text">*</span>직위</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputMemberPosition" placeholder="직위" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputMemberDepartment" class="col-sm-3 control-label">부서</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputMemberDepartment" placeholder="부서">
                </div>
              </div>

              <div class="form-group">
                <label for="inputMemberPhoto" class="col-sm-12 control-label">
                  사진
                  <div class="help-block pull-right">　(권장사이즈 : 68 * 68, png 파일)</div>
                </label>
                <div class="col-sm-12">
                  <input type="file" class="form-control form-control-file" name="inputMemberPhoto">
                </div>
              </div>

            </div>
          </div>
          <span class="space"></span>

          <div class="form-group text-center">
            <button type="submit" class="btn btn-primary" onclick="return checkDuplicate()">등록 <i class="fa fa-check spaceLeft"></i></button>
            <span class="space"></span>
            <button type="button" class="btn btn-warning" onclick="location.href='manageMember.jsp'" >취소<i class="fa fa-times spaceLeft"></i> </button>
          </div>
          <span class="space"></span>
    </form>
  </div>

  <script src="js/jquery.js"></script>
  <script src="js/bootstrap-transition.js"></script>
  <script src="js/bootstrap-alert.js"></script>
  <script src="js/bootstrap-modal.js"></script>
  <script src="js/bootstrap-dropdown.js"></script>
  <script src="js/bootstrap-scrollspy.js"></script>
  <script src="js/bootstrap-tab.js"></script>
  <script src="js/bootstrap-tooltip.js"></script>
  <script src="js/bootstrap-popover.js"></script>
  <script src="js/bootstrap-button.js"></script>
  <script src="js/bootstrap-collapse.js"></script>
  <script src="js/bootstrap-carousel.js"></script>
  <script src="js/bootstrap-typeahead.js"></script>
  <script type="text/javascript">
    var checkValue=0;
    var sValue = document.addMember.selectBox;

    function selectMembershipName(){
      var length = "<%=temp.length%>";
      var array= new Array(parseInt("<%=temp.length%>"));

      for(var a=0;a<parseInt("<%=temp.length%>");a++){
        array[a]=new Array(5);
      }
      if(sValue.value == "0"){
        document.addMember.inputMembershipLevel.value="";
        document.addMember.inputMembershipMainItem.value="";
        document.addMember.inputMembershipDivision.value="";
        document.addMember.inputMembershipHomePage.value="";
      }  else{
        <%
          int num=0;
        %>
        if(parseInt("<%=count%>")==0){
        <% for(int k=0;k<temp.length;k++){ %>
          array["<%=k%>"][0]="<%=temp[num][0]%>";
          array["<%=k%>"][1]="<%=temp[num][1]%>";
          array["<%=k%>"][2]="<%=temp[num][2]%>";
          array["<%=k%>"][3]="<%=temp[num][3]%>";
          array["<%=k%>"][4]="<%=temp[num][4]%>";
        <%
          num++;
        }
        count++;
        %>
        for(var k=0;k<"<%=temp.length%>";k++){
          if(array[k][0]==sValue.value){
            document.addMember.inputMembershipLevel.value= array[k][1];
            document.addMember.inputMembershipMainItem.value=array[k][2]//"%=smain_item%>";
            document.addMember.inputMembershipDivision.value=array[k][3]//"%stype%>";
            document.addMember.inputMembershipHomePage.value=array[k][4]//"%shomepage%>";
          }
        }
        }
        else{
          for(var k=0;k<"<%=temp.length%>";k++){
            if(array[k][0]==sValue.value){
              document.addMember.inputMembershipLevel.value= array[k][1];
              document.addMember.inputMembershipMainItem.value=array[k][2]//"%=smain_item%>";
              document.addMember.inputMembershipDivision.value=array[k][3]//"%stype%>";
              document.addMember.inputMembershipHomePage.value=array[k][4]//"%shomepage%>";
            }
          }
        }
      }
    }

    function submit_EMAIL() {
    var semail = document.addMember.inputMemberEmail.value;

    if(semail == "") {
        alert("이메일을 입력해주세요.");
      } else {
        window.open("duplicate_member_email.jsp?inputMemberEmail="+semail,"","width=450 height=135");
      }
    }
    function checkDuplicate() {
      if(checkValue == 0) {
        alert("이메일 중복을 확인해 주세요.");
        return false;
      }
      else if(checkValue == 1) {
        alert("중복된 이메일입니다. 이메일을 변경해주세요.");
        return false;
      }  else if(sValue.value  ==  "0"){
        alert("회원사 레벨을 선택해 주세요.");
        return false;
      } else {
        return true;
      }
    }
    function ok() {
      checkValue = 2;
    }
    function no() {
      checkValue = 1;
    }
  </script>
 </body>
</html>
