<%@page contentType="text/html; charset=euc-kr" language="java" errorPage=""%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy,jxl.*"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFCell"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFRow"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFSheet"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="org.apache.poi.xssf.usermodel.XSSFCell"%>
<%@page import="org.apache.poi.xssf.usermodel.XSSFRow"%>
<%@page import="org.apache.poi.xssf.usermodel.XSSFSheet"%>
<%@page import="org.apache.poi.xssf.usermodel.XSSFWorkbook"%>
<%

String savePath = request.getRealPath("");
String temp = "upload";
String realPath = savePath+"/"+temp;
int sizeLimit = 10 * 1024 * 1024;

MultipartRequest multi = new MultipartRequest(request, realPath, sizeLimit, "utf-8", new DefaultFileRenamePolicy());
String filename = realPath+"/"+multi.getFilesystemName("uploadInput");

java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
String event_update = formatter.format(new java.util.Date());

String org_id = (String)session.getAttribute("id");
Connection myConn = null;
PreparedStatement pstmt = null;
String mySQL = null;
String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
String db_user = "intern";
String db_password = "password";
String Driver="org.mariadb.jdbc.Driver";

try{
  Class.forName(Driver);
  myConn = DriverManager.getConnection(db_url, db_user, db_password);
}catch(SQLException ex){
    System.err.println("SQLException: " + ex.getMessage());
}

FileInputStream fis=new FileInputStream(filename);
XSSFWorkbook workbook=new XSSFWorkbook(fis);
int rowindex=0;
int columnindex=0;
//시트 수 (첫번째에만 존재하므로 0을 준다)
//만약 각 시트를 읽기위해서는 FOR문을 한번더 돌려준다
XSSFSheet sheet=workbook.getSheetAt(0);
//행의 수
int rows=sheet.getPhysicalNumberOfRows();
int pstmt_number=1;
for(rowindex=1;rowindex<rows;rowindex++){
    //행을읽는다
    XSSFRow row=sheet.getRow(rowindex);
    if(row !=null){
        //셀의 수

        int cells=row.getPhysicalNumberOfCells();

        mySQL ="Insert into Member(MEM_EMAIL, MEMBERSHIP_NAME, MEM_NAME, MEM_DEPARTMENT, MEM_POSITION, MEM_PHONE, MEM_TEL, MEM_UPDATE, ORG_ID)values(?,?,?,?,?,?,?,?,?)";
        pstmt = myConn.prepareStatement(mySQL);

        String [] insert = new String[cells];
        for(columnindex=0;columnindex<=cells;columnindex++){
            XSSFCell cell=row.getCell(columnindex);
            String value="";
            //셀이 빈값일경우를 위한 널체크
            if(cell==null){
                continue;
            }else{
                //타입별로 내용 읽기
                switch (cell.getCellType()){
                case XSSFCell.CELL_TYPE_FORMULA:
                    value=cell.getCellFormula();

                    break;
                case XSSFCell.CELL_TYPE_NUMERIC:
                    value=""+(int)cell.getNumericCellValue();

                    break;
                case XSSFCell.CELL_TYPE_STRING:
                    value=""+cell.getStringCellValue();
                    break;
                case XSSFCell.CELL_TYPE_BLANK:
                    //value=cell.getBooleanCellValue();
                    value="";
                    break;
                case XSSFCell.CELL_TYPE_ERROR:
                    value=""+cell.getErrorCellValue();

                    break;
                }
            }
          pstmt.setString(pstmt_number++, value.trim());
        }pstmt.setString(pstmt_number++, event_update);
        pstmt.setString(pstmt_number++, org_id);

        pstmt_number=1;
        try{
        pstmt.executeUpdate();
      }catch (SQLException ex) {
            //System.err.println("SQLException: " + ex.getMessage());
            %>
            <script>
            alert("<%=ex.getMessage()%>");
            </script>
            <%
         }
    }
}
%>
<script>
window.close();
</script>
