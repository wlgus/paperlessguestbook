<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>
<%@ include file = "session.jsp"%>

<%
  String radioValue= new String(request.getParameter("editMember").getBytes("8859_1"),"utf-8");
%>

 <!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>회원편집 &middot; FORCS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/making.css" rel="stylesheet">
  <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-memberInfo {
      max-width: 900px;
      padding: 0px;
      margin: 20px auto;
      background-color: #fff;
      border: 1px solid #e5e5e5;
      -webkit-border-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

    .form-memberInfo .form-memberInfo-heading {
      color: white;
      padding: 50px 0;
      margin-top: 0px;
      margin-bottom: 45px;
      background-color: rgb(33, 47, 61);
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
    }

    .form-memberInfo input[type="text"],
    .form-memberInfo input[type="password"],
    .form-memberInfo input[type="file"],
    .form-memberInfo input[type="tel"],
    .form-memberInfo input[type="email"],
    .form-memberInfo select {
      font-size: 15px;
      height: auto;
      margin-bottom: 25px;
      padding: 7px 9px;
    }

  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
  <%
   Connection myConn = null;
   Statement stmt = null;
   ResultSet myResultSet;

   String mySQL;
   String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
   String db_user = "intern";
   String db_password = "password";
   String Driver = "org.mariadb.jdbc.Driver";

   String membership_name=null;
   String mem_name=null;
   String mem_phone=null;
   String mem_email=null;
   String mem_picture=null;
   String mem_department=null;
   String mem_position=null;
   String mem_tel=null;

     try {
       Class.forName(Driver);
        myConn = DriverManager.getConnection(db_url, db_user, db_password);
        stmt = myConn.createStatement();  //include가 안되어있기때문에 해줘야한다.

     } catch (SQLException ex) {
        System.err.println("SQLException: "+ ex.getMessage());
     }

     mySQL = "select MEMBERSHIP_NAME, MEM_NAME, MEM_PHONE, MEM_EMAIL, MEM_PICTURE, MEM_DEPARTMENT, MEM_POSITION, MEM_TEL from Member where ORG_ID = '" + session_id +"' and MEM_NAME ='"+radioValue+"'";
     myResultSet = stmt.executeQuery(mySQL);
     if(myResultSet.next()) {

        membership_name = myResultSet.getString("MEMBERSHIP_NAME");
        mem_name = myResultSet.getString("MEM_NAME");
        mem_phone = myResultSet.getString("MEM_PHONE");
        mem_email = myResultSet.getString("MEM_EMAIL");
        mem_picture = myResultSet.getString("MEM_PICTURE");
        mem_department = myResultSet.getString("MEM_DEPARTMENT");
        mem_position = myResultSet.getString("MEM_POSITION");
        mem_tel = myResultSet.getString("MEM_TEL");
      }
  %>
  <div class="container-fluid">
    <form name="editMember" action="editMember_verify.jsp" class="form-memberInfo ">

          <div class="row center-block">
            <button type="button" class="arrow-btn btn-link" onClick="location.href='manageMember.jsp'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
            <h3 class="form-memberInfo-heading text-center">회원 편집</h3>
          </div>

          <div class="row center-block">
            <div class="col-sm-6 border">
              <h4><b>기본 정보</b></h4><span class="space"></span>

              <div class="form-group">
                <label for="inputMembershipName" class="col-sm-3 control-label col-xs-12"><span class="red-color-text">*</span>회사명</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="inputMembershipLevel" value="<%=membership_name%>" disabled>
                </div>
              </div>

              <div class="form-group">
                <label for="inputMemberName" class="col-sm-3 control-label"><span class="red-color-text">*</span>이름</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputMemberName" value="<%=mem_name%>" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputMemberPhone" class="col-sm-3 control-label"><span class="red-color-text">*</span>휴대전화</label>
                <div class="col-sm-9">
                  <input type="tel" class="form-control" name="inputMemberPhone" placeholder="'-' 없이 숫자만 입력해주세요" value="<%=mem_phone%>" required>
                </div>
              </div>

            </div>
            <div class="col-sm-6 border">
             <h4 class="hidden-xs">　</h4><span class="space"></span>


             <div class="form-group">
               <label for="inputMemberEmail" class="col-sm-3 control-labe col-xs-12"><span class="red-color-text">*</span>이메일</label>
               <div class="col-sm-7 col-xs-10">
                 <input type="email" class="form-control" name="inputMemberEmail" placeholder="이메일" value="<%=mem_email%>" required>
               </div>
               <div class="col-sm-2 col-xs-2">
                 <button type="button" class="btn btn-sm btn-primary space-remove" onClick="submit_email()">중복</button>
               </div>
             </div>

             <div class="form-group">
                 <input type="text" class="hidden form-control" name="original" value="<%=mem_email%>">
             </div>

             <div class="form-group">
               <label for="inputMemberPhoto" class="col-sm-12 control-label">
                 사진
                 <div class="help-block pull-right">　(권장사이즈 : 68 * 68, png 파일)</div>
               </label>
               <div class="col-sm-12">
                 <input type="file" class="form-control form-control-file" name="inputMemberPhoto" value="<%=mem_picture%>">
               </div>
             </div>

            </div>
          </div>

          <div class="row center-block">
            <div class="col-sm-6 border">
              <h4><b>추가 정보</b></h4><span class="space"></span>

              <div class="form-group">
                <label for="inputMemberDepartment" class="col-sm-3 control-label">부서</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputMemberDepartment" placeholder="부서" value="<%=mem_department%>">
                </div>
              </div>

              <div class="form-group">
                <label for="inputMemberPosition" class="col-sm-3 control-label">직위</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputMemberPosition" placeholder="직위" value="<%=mem_position%>">
                </div>
              </div>

            </div>
            <div class="col-sm-6 border">
              <h4 class="hidden-xs">　</h4><span class="space"></span>

              <div class="form-group">
                <label for="inputMemberTel" class="col-sm-3 control-label">회사전화</label>
                <div class="col-sm-9">
                  <input type="tel" class="form-control" name="inputMemberTel" placeholder="'-' 없이 숫자만 입력해주세요" value="<%=mem_tel%>">
                </div>
              </div>

              <div class=" hidden form-group">
                <div class="hidden col-sm-7 col-xs-10">
                  <input type="text" class="hidden form-control" name="inputOriginalName"  value="<%=radioValue%>" required>
                </div>
              </div>

          </div>
        </div>
        <span class="space"></span>

        <div class="form-group text-center">
          <button type="submit" class="btn btn-primary" onClick="return check()">수정 <i class="fa fa-check spaceLeft"></i></button>
          <span class="space"></span>
          <button type="button" class="btn btn-warning" onClick="delete_member()">삭제 <i class="fa fa-times spaceLeft"></i></button>
        </div>
        <span class="space"></span>
  </form>
</div>
<script src="js/jquery.js"></script>
<script src="js/bootstrap-transition.js"></script>
<script src="js/bootstrap-alert.js"></script>
<script src="js/bootstrap-modal.js"></script>
<script src="js/bootstrap-dropdown.js"></script>
<script src="js/bootstrap-scrollspy.js"></script>
<script src="js/bootstrap-tab.js"></script>
<script src="js/bootstrap-tooltip.js"></script>
<script src="js/bootstrap-popover.js"></script>
<script src="js/bootstrap-button.js"></script>
<script src="js/bootstrap-collapse.js"></script>
<script src="js/bootstrap-carousel.js"></script>
<script src="js/bootstrap-typeahead.js"></script>
<script type="text/javascript">
  var flag=0;
  var checkValue=0;
  function submit_email() {
  var semail = document.getElementsByName("inputMemberEmail")[0].value;
  var original = document.getElementsByName("original")[0].value;

  if(semail=="") {
      alert("이메일을 입력해주세요.");
    } else {
      flag=1;
      var url = "duplicate_member_email_edit.jsp?inputMemberEmail="+semail+"&original="+original;
      window.open(url,"","width=450 height=135");
    }
  }
  function check(){
    if(flag == 0) {
      alert("이메일 중복을 확인 해주세요.");
      return false;
    }
    else if(checkValue==1){
      alert("중복된 이메일입니다. 이메일을 변경해 주세요.");
      return false;
    }
  }
  function ok(){
    checkValue = 2;
  }
  function no(){
    checkValue = 1;
  }
  function delete_member() {
    var demail=document.editMember.inputMemberEmail.value;
    window.open("delete_member.jsp?inputMemberEmail="+demail,"","width=450 height=135");
    location.href="manageMember.jsp";
  }
</script>
</body>
</html>
