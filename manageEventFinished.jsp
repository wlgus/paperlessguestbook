<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>
<%@ include file = "session.jsp"%>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>행사관리 &middot; FORCS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/making.css" rel="stylesheet">
  <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-manageEvent {
      max-width: 900px;
      padding: 0px 0px 20px 0px;
      margin: 20px auto;
      background-color: #fff;
      border: 1px solid #e5e5e5;
      -webkit-border-top-right-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

    .form-manageEvent .form-manageEvent-heading {
      color: white;
      padding: 50px 0;
      margin-top: 0px;
      margin-bottom: 35px;
      background-color: rgb(33, 47, 61);
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
    }

    .event-list {
        border: 1px solid rgb(224, 224, 224);
        border-radius: 5px;
        margin-top: 15px;
        margin-bottom: 15px;
    }

    .event-thumbnail {
      padding: 0px;
      border-radius: 5px;
      margin: 0px 0px 15px 0px;
    }

    .border-custom {
      border-left: 1px solid rgb(224, 224, 224);
      padding-left: 0 5px;
      font-weight: bold;
      margin: 0px auto;
      color: rgb(88, 88, 88);
    }

    .img-resizing{
        display: inline-block;
        max-width: 100%;
        height: auto;
    }

    .event-image-resizing{
      width: 450px;
      height: 150px;
    }

  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
  <%!
     public Integer toInt(String x){
        int a = 0;
        try{
           a = Integer.parseInt(x);
        }catch(Exception e){}
        return a;
     }
  %>
  <div class="container-fluid">
    <div action="" class="form-manageEvent ">

          <div class="row center-block">
            <button type="button" class="arrow-btn btn-link" onclick="location.href='main.jsp'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
            <button type="button" class="search-btn btn-link" onclick="location.href='searchEvent.jsp'"><i class="fa fa-search" aria-hidden="true"></i></button>
            <h3 class="form-manageEvent-heading text-center">
              행사 관리
            </h3>
          </div>

          <div class="row center-block">

            <div class="col-sm-12 border">
              <div class="">
                <div class="pull-right">
                  <button type="button" class="btn btn-link btn-sm" onclick="location.href='addEvent.jsp'">행사 등록 <i class="fa fa-plus-square" aria-hidden="true"></i></button>
                </div>
                <div class="" align="">
                  <div class="btn-group" role="group" aria-label="">
                    <button type="button" class="btn  btn-primary btn-sm" onclick="location.href='manageEventOngoing.jsp'">진행중</button>
                    <button type="button" class="btn btn-info  btn-sm" onclick="location.href='manageEventFinished.jsp'">완료</button>
                  </div>
                </div>
              </div>
              <br>
              <%
              int pageno = toInt(request.getParameter("pageno"));
              if(pageno<1)
              {
                pageno =1;
              }
                  Connection myConn = null;
                  Statement stmt = null;
                  ResultSet myResultSet;
                  ResultSet paging;
                  String mySQL = null;
                  String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
                  String db_user = "intern";
                  String db_password = "password";
                  String Driver="org.mariadb.jdbc.Driver";
                  int num=0;
                  String event_title;
                  String event_s_d;
                  String event_s_t;
                  String event_e_d;
                  String event_e_t;
                  String event_image;
                  int total_record=0;
                  int page_per_record_cnt=6;
                  int page_group=0; // 총 몇개의 페이지인지
                  int view_page = 0; // 보여주기 시작할 위치

                  if(pageno == 1)
                  {
                    view_page =0;
                  }else{
                    view_page= (pageno-1)*page_per_record_cnt;
                  }

                  try{
                    Class.forName(Driver);
                    myConn = DriverManager.getConnection(db_url, db_user, db_password);

                  }catch(SQLException ex){
                    System.err.println("SQLException: " + ex.getMessage());
                  }
                  mySQL = "select TITLE, EVENT_S_D, EVENT_S_T, EVENT_E_D, EVENT_E_T, EVENT_IMAGE  from Event where ORG_ID = '"+ session_id +"' and EVENT_STATUS = '완료'";
                  stmt=myConn.createStatement();
                  paging = stmt.executeQuery(mySQL);
                  paging.last();
                  total_record = paging.getRow(); // 전체 레코드 수

                  mySQL = "select TITLE, EVENT_S_D, EVENT_S_T, EVENT_E_D, EVENT_E_T, EVENT_IMAGE  from Event where ORG_ID = '"+ session_id +"' and EVENT_STATUS = '완료' limit "+view_page+","+page_per_record_cnt+"";
                  stmt=myConn.createStatement();
                  myResultSet=stmt.executeQuery(mySQL);

                  if(myResultSet != null) {
                  while(myResultSet.next()){
                    num++;

                    event_title = myResultSet.getString("TITLE");
                    event_s_d = myResultSet.getString("EVENT_S_D");
                    event_s_t = myResultSet.getString("EVENT_S_T");
                    event_e_d = myResultSet.getString("EVENT_E_D");
                    event_e_t = myResultSet.getString("EVENT_E_T");
                    event_image = myResultSet.getString("EVENT_IMAGE");
              %>
              <div class="col-md-4 col-sm-6 col-xs-6">
                <div class="event-list">
                  <div class="event-thumbnail">
                      <a href="#">
                          <img class="img-responsive event-image-resizing" src="<%=event_image%>" alt="">
                      </a>
                  </div>
                  <div>
                      <p class="text-center">
                      <b><%=event_title%></b>
                      </p>
                      <p class="text-center">
                      <span><%=event_s_d%> </span>~<span> <%=event_e_d%> </span>
                      </p>
                      <hr/>
                      <p class="text-center">
                        <span class="hidden-xs"><a href="#"><img class="img-resizing" src="img/guestbook.png" alt="방명록"></a></span>
                        <span class="border-custom hidden-xs"><a href="manageParticipantFinished.jsp?event_title=<%=event_title%>" method="post"><img class="img-resizing" src="img/manageParticipants.png" alt="참석자 관리"></a></span>
                        <span class="border-custom hidden-xs"><a href="editEvent.jsp?event_title=<%=event_title%>" method="post"><img class="img-resizing" src="img/editEvents.png" alt="행사 편집"></a></span>
                        <div class="visible-xs">
                          <center>
                            <button type="button" name="button" class="btn btn-link btn-xs">방명록</button>
                            <button type="button" name="button" class="btn btn-link btn-xs">참석자 관리</button>
                            <button type="button" name="button" class="btn btn-link btn-xs">행사 편집</button>
                          </center>
                        </div>
                      </p>
                  </div>
                </div>
              </div>
              <%
               }
              } myResultSet.close();
              stmt.close();
              if(total_record % page_per_record_cnt != 0) // 남는 레코드 수 존재
              {
                page_group = (total_record/page_per_record_cnt)+1;
              }else{
                page_group = total_record / page_per_record_cnt;
              }

               int group_per_page_cnt  = 5;     //페이지 당 보여줄 번호 수
               int record_end_no = pageno*page_per_record_cnt;
               int record_start_no = record_end_no-(page_per_record_cnt-1);
               if(record_end_no>total_record){
                  record_end_no = total_record;
               }

               int total_page = total_record / page_per_record_cnt + (total_record % page_per_record_cnt>0 ? 1 : 0);
               if(pageno>total_page){
                  pageno = total_page;
               }
               int group_no = pageno/group_per_page_cnt+( pageno%group_per_page_cnt>0 ? 1:0);
               int page_eno = group_no*group_per_page_cnt;
               int page_sno = page_eno-(group_per_page_cnt-1);

               if(page_sno<1)
               {
                 page_sno=1;
               }

               if(page_eno>total_page){
                  page_eno=total_page;
               }

               int prev_pageno = page_sno-group_per_page_cnt;
               int next_pageno = page_sno+group_per_page_cnt;
               if(prev_pageno<1){
                  prev_pageno=1;
               }
               if(next_pageno>total_page){
                  next_pageno=total_page/group_per_page_cnt*group_per_page_cnt+1;
               }
              %>
            </div>

            <div class="text-center">
              <ul class="pagination">
                <li><a href="manageEventFinished.jsp?pageno=<%=prev_pageno%>">이전</a></li>
                <%for(int i =page_sno;i<=page_eno;i++){%>
                  <li><a href="manageEventFinished.jsp?pageno=<%=i %>">
                    <%if(pageno == i){ %>
                      <%=i %>
                    <%}else{ %>
                      <%=i %>
                    <%} %>
                  </a></li>
                  <%if(i<page_eno){ %>
                  <%} %>
                <%} %>
                <li><a href="manageEventFinished.jsp?pageno=<%=next_pageno%>" >다음</a></li>
              </ul>
            </div>

          </div>
          </div>
        </div>
  </div>
  </div>
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap-transition.js"></script>
  <script src="js/bootstrap-alert.js"></script>
  <script src="js/bootstrap-modal.js"></script>
  <script src="js/bootstrap-dropdown.js"></script>
  <script src="js/bootstrap-scrollspy.js"></script>
  <script src="js/bootstrap-tab.js"></script>
  <script src="js/bootstrap-tooltip.js"></script>
  <script src="js/bootstrap-popover.js"></script>
  <script src="js/bootstrap-button.js"></script>
  <script src="js/bootstrap-collapse.js"></script>
  <script src="js/bootstrap-carousel.js"></script>
  <script src="js/bootstrap-typeahead.js"></script>
 </body>
</html>
