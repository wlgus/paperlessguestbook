<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html>
  <head>
   <meta charset="utf-8">
   <title>회원 엑셀추가 &middot; FORCS</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="description" content="">
   <meta name="author" content="">
   <link href="css/bootstrap.css" rel="stylesheet">
   <link href="css/making.css" rel="stylesheet">
   <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
   <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
   <style type="text/css">
     body {
       padding-top: 40px;
       padding-bottom: 40px;
       background-color: #fff;
     }
     .file-box {
        border: 1px solid #c4c4c4;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
       padding: 5px;
       margin-bottom: 20px;
     }
     .outer {
        display: table;
        width: 100%;
        height: 100%;
      }
      .inner {
        display: table-cell;
        vertical-align: middle;
        text-align: center;
      }
      .centered {
        position: relative;
        display: inline-block;
        width: 30%;
        padding: 5px 0px 30px 0px;

        margin: 20px auto;
        background-color: #f5f5f5;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        text-align: center;
      }
      .heading {
        margin:0 0 30px 0;
      }
   </style>
   <link href="css/bootstrap-responsive.css" rel="stylesheet">
  </head>
  <body>
   <div class="outer">
     <div class="inner">
       <div class="centered">
         <form name="addMember" action="addMember_excel_verify.jsp" method="post" enctype="multipart/form-data">
           <div class="form-group">
             <div class="col-md-8 col-md-offset-2 col-sm-12">
               <h4 class="heading  hidden-xs"><b><i class="fa fa-table" aria-hidden="true"></i> 엑셀추가</b></h4>
               <h5 class="heading visible-xs"><b> 엑셀추가</b></h5>
               <input type="file" class="col-sm-12 file-box" id="uploadInput" name="uploadInput">
                 <div class="space">
              <button type="submit" class="col-sm-6 col-sm-offset-3 btn btn-info btn-xs">제출</button>
             </div>
           </div>
         </form>
       </div>
     </div>
   </div>
   <script type="text/javascript">

   </script>
  </body>
</html>
