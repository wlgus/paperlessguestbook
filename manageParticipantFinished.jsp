﻿<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>
<%@ include file = "session.jsp"%>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>참석자 관리 &middot; FORCS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/making.css" rel="stylesheet">
  <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-manageParticipant {
      max-width: 900px;
      padding: 0px;
      margin: 20px auto;
      background-color: #fff;
      border: 1px solid #e5e5e5;
      -webkit-border-top-right-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

    .form-manageParticipant .form-manageParticipant-heading {
      color: white;
      padding: 50px 0;
      margin-top: 0px;
      margin-bottom: 35px;
      background-color: rgb(33, 47, 61);
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
    }

    .form-manageParticipant input[type="text"],
    .form-manageParticipant input[type="password"],
    .form-manageParticipant input[type="file"],
    .form-manageParticipant input[type="tel"],
    .form-manageParticipant input[type="email"] {
      font-size: 15px;
      height: auto;
      margin-bottom: 25px;
      padding: 7px 9px;
    }

    .table-font-size {
      font-size: 12px;
    }

  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
  <%!
     public Integer toInt(String x){
        int a = 0;
        try{
           a = Integer.parseInt(x);
        }catch(Exception e){}
        return a;
     }
  %>
  <%
    String event_title = new String(request.getParameter("event_title").getBytes("8859_1"),"utf-8");
  %>
  <div class="container-fluid">
    <form action="delete_participant.jsp?event_title=<%=event_title%>" class="form-manageParticipant" method="post">

          <div class="row center-block">
            <button type="button" class="arrow-btn btn-link" onclick="location.href='manageEventOngoing.jsp'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
            <button type="button" class="search-btn btn-link" onclick="location.href='searchParticipant.jsp?event_title=<%=event_title%>'"><i class="fa fa-search" aria-hidden="true"></i></button>
            <h3 class="form-manageParticipant-heading text-center">
              <%=event_title%> 참석자 관리
            </h3>
          </div>

          <div class="row center-block">
            <div class="col-sm-12 border text-center">
              <h4><b>완료된 행사입니다.</b></h4><span class="space"></span>
              <div class="" align="right">

              </div>
              <br>
              <div class="table-responsive">
                <table class="table table-hover table-bordered">
                  <thead>
                    <tr>
                      <th class="text-center"><b>No.</b></th>
                      <th class="text-center"><b>소속</b></th>
                      <th class="text-center"><b>부서</b></th>
                      <th class="text-center"><b>이름</b></th>
                      <th class="text-center"><b>직위</b></th>
                      <th class="text-center"><b>연락처</b></th>
                      <th class="text-center"><b>이메일</b></th>
                    </tr>
                  </thead>
                  <%

                  int pageno = toInt(request.getParameter("pageno"));
                  if(pageno<1)
                  {
                    pageno =1;
                  }

                    Connection myConn = null;
                    Statement stmt = null;
                    ResultSet rs;
                    ResultSet paging;
                    String mySQL = null;
                    String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
                    String db_user = "intern";
                    String db_password = "password";
                    String Driver="org.mariadb.jdbc.Driver";
                    int number =0;
                    int total_record=0;
                    int page_per_record_cnt=10;
                    int page_group=0; // 총 몇개의 페이지인지
                    int view_page = 0; // 보여주기 시작할 위치

                    if(pageno == 1)
                    {
                      view_page =0;
                    }else{
                      view_page= (pageno-1)*page_per_record_cnt;
                    }

                    try{
                      Class.forName(Driver);
                      myConn = DriverManager.getConnection(db_url, db_user, db_password);
                      stmt = myConn.createStatement();
                    }catch(SQLException ex){
                      System.err.println("SQLException: " + ex.getMessage());
                    }
                    mySQL = "select GUEST_EMAIL, GUEST_COMPANY, GUEST_DEPARTMENT, GUEST_POSITION, GUEST_NAME, GUEST_PHONE from Guestbook where ORG_ID = '" + session_id + "' and Guestbook.TITLE ='"+event_title +"'";
                    stmt=myConn.createStatement();
                    paging = stmt.executeQuery(mySQL);
                    paging.last();
                    total_record = paging.getRow(); // 전체 레코드 수

                    mySQL = "select GUEST_EMAIL, GUEST_COMPANY, GUEST_DEPARTMENT, GUEST_POSITION, GUEST_NAME, GUEST_PHONE from Guestbook where ORG_ID = '" + session_id + "' and Guestbook.TITLE ='"+event_title +"' limit "+view_page+","+page_per_record_cnt+"";
                    rs = stmt.executeQuery(mySQL);

                    if(rs != null) {
                    while(rs.next()){
                      number++;
                      String company = rs.getString("GUEST_COMPANY");
                      String departmant = rs.getString("GUEST_DEPARTMENT");
                      String name = rs.getString("GUEST_NAME");
                      String position = rs.getString("GUEST_POSITION");
                      String phone = rs.getString("GUEST_PHONE");
                      String email = rs.getString("GUEST_EMAIL");
                  %>
                  <tbody class="table-font-size">
                    <tr>
                      <td class="text-center"><%=number%></td>
                      <td class="text-center"><%=company%></td>
                      <td class="text-center"><%=departmant%></td>
                      <td class="text-center"><%=name%></td>
                      <td class="text-center"><%=position%></td>
                      <td class="text-center"><%=phone%></td>
                      <td class="text-center"><%=email%></td>
                    </tr>
                  </tbody>
                  <%
                      }
                    } rs.close();
                     stmt.close();
                     if(total_record % page_per_record_cnt != 0) // 남는 레코드 수 존재
                     {
                       page_group = (total_record/page_per_record_cnt)+1;
                     }else{
                       page_group = total_record / page_per_record_cnt;
                     }

                      int group_per_page_cnt  = 5;     //페이지 당 보여줄 번호 수

                      int record_end_no = pageno*page_per_record_cnt;
                      int record_start_no = record_end_no-(page_per_record_cnt-1);
                      if(record_end_no>total_record){
                         record_end_no = total_record;
                      }

                      int total_page = total_record / page_per_record_cnt + (total_record % page_per_record_cnt>0 ? 1 : 0);
                      if(pageno>total_page){
                         pageno = total_page;
                      }
                      int group_no = pageno/group_per_page_cnt+( pageno%group_per_page_cnt>0 ? 1:0);
                      int page_eno = group_no*group_per_page_cnt;
                      int page_sno = page_eno-(group_per_page_cnt-1);

                      if(page_sno<1)
                      {
                        page_sno=1;
                      }

                      if(page_eno>total_page){
                         page_eno=total_page;
                      }

                      int prev_pageno = page_sno-group_per_page_cnt;
                      int next_pageno = page_sno+group_per_page_cnt;
                      if(prev_pageno<1){
                         prev_pageno=1;
                      }
                      if(next_pageno>total_page){
                         next_pageno=total_page/group_per_page_cnt*group_per_page_cnt+1;
                      }
                     %>

                </table>
                <p class="block_p">
                  <div class="text-center">
                    <ul class="pagination">
                      <li><a href="manageParticipant.jsp?pageno=<%=prev_pageno%>&event_title=<%=event_title%>">이전</a></li>
                      <%for(int i =page_sno;i<=page_eno;i++){%>
                        <li><a href="manageParticipant.jsp?pageno=<%=i %>&event_title=<%=event_title%>">
                          <%if(pageno == i){ %>
                            <%=i %>
                          <%}else{ %>
                            <%=i %>
                          <%} %>
                        </a></li>
                        <%if(i<page_eno){ %>
                        <%} %>
                      <%} %>
                      <li><a href="manageParticipant.jsp?pageno=<%=next_pageno%>&event_title=<%=event_title%>" >다음</a></li>
                    </ul>
                  </div>
                </p>
              </div>
            </div>
            </div>
          </div>
    </form>
  </div>
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap-transition.js"></script>
  <script src="js/bootstrap-alert.js"></script>
  <script src="js/bootstrap-modal.js"></script>
  <script src="js/bootstrap-dropdown.js"></script>
  <script src="js/bootstrap-scrollspy.js"></script>
  <script src="js/bootstrap-tab.js"></script>
  <script src="js/bootstrap-tooltip.js"></script>
  <script src="js/bootstrap-popover.js"></script>
  <script src="js/bootstrap-button.js"></script>
  <script src="js/bootstrap-collapse.js"></script>
  <script src="js/bootstrap-carousel.js"></script>
  <script src="js/bootstrap-typeahead.js"></script>
  <script LANGUAGE="JavaScript">

function checkValue(){
  var temp = document.getElementsByName("cancel");
  var length = temp.length;
  var flag=0;

  for(var i=0;i<length;i++){
    if(temp[i].checked){
      flag=1;
    }
  }

  if(temp[0]==undefined){
    alert("삭제할 참석자가 존재하지 않습니다.");
    return false;
  }
  else if(flag==0){
    alert("삭제할 참석자를 선택해주세요.");
    return false;
  }
  else{
    return true;
  }
}
    </script>
 </body>
</html>
