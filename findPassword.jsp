﻿<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>비밀번호찾기 &middot; FORCS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/making.css" rel="stylesheet">
  <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-findPassword {
      max-width: 900px;
      padding: 0px;
      margin: 20px auto;
      background-color: #fff;
      border: 1px solid #e5e5e5;
      -webkit-border-top-right-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

    .form-findPassword .form-findPassword-heading {
      color: white;
      padding: 50px 0;
      margin-top: 0px;
      margin-bottom: 35px;

      background-color: rgb(33, 47, 61);
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
    }

    .form-findPassword input[type="text"],
    .form-findPassword input[type="password"],
    .form-findPassword input[type="file"],
    .form-findPassword input[type="tel"],
    .form-findPassword input[type="email"] {
      font-size: 15px;
      height: auto;
      margin-bottom: 25px;
      padding: 7px 9px;
    }

    .form-findPassword-2 {
      padding: 30px 20px;
      margin: 20px auto 40px;
      background-color: #f5f5f5;
      border: 1px solid #e5e5e5;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
  <div class="container-fluid">
    <div class="form-findPassword ">

          <div class="row center-block">
            <button type="submit" class="arrow-btn btn-link" onClick="location.href='signIn.jsp'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
            <h3 class="form-findPassword-heading text-center">
              비밀번호 찾기
            </h3>
          </div>

          <div class="row center-block">
            <div class="col-sm-offset-3 col-sm-6 border">
              <form action="findPassword_verify.jsp" class="form-findPassword-2" method="post">
                <center><h4 class="hidden-xs"><b>이메일, 휴대전화를 입력해주세요</b></h4></center>
                <center><h5 class="visible-xs"><b>이메일, 휴대전화를 입력해주세요</b></h5></center>
                <span class="space"></span>
                <input type="text" class="input-block-level" name="inputAdminEmail" placeholder="Email address">
                <input type="text" class="input-block-level" name="inputAdminPhone" placeholder="Phone number">
                <button class="btn btn-large btn-primary btn-block" type="submit" onClick="location.href='findPassword_verify.jsp'">검색</button>
                <span class="space"></span>
                <center><img class="img-responsive" src="img/logo.png" alt="logo"></center>
              </form>
            </div>
            </div>
          </div>

    </div>
  </div>
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap-transition.js"></script>
  <script src="js/bootstrap-alert.js"></script>
  <script src="js/bootstrap-modal.js"></script>
  <script src="js/bootstrap-dropdown.js"></script>
  <script src="js/bootstrap-scrollspy.js"></script>
  <script src="js/bootstrap-tab.js"></script>
  <script src="js/bootstrap-tooltip.js"></script>
  <script src="js/bootstrap-popover.js"></script>
  <script src="js/bootstrap-button.js"></script>
  <script src="js/bootstrap-collapse.js"></script>
  <script src="js/bootstrap-carousel.js"></script>
  <script src="js/bootstrap-typeahead.js"></script>
 </body>
</html>
