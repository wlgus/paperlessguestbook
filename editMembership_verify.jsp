<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>

<%
String org_id = (String)session.getAttribute("id");
String membership_original_name =new String(request.getParameter("inputOriginalName").getBytes("8859_1"),"utf-8");
String membership_name =new String(request.getParameter("inputMembershipName").getBytes("8859_1"),"utf-8");
String membership_type = new String(request.getParameter("inputMembershipDivision").getBytes("8859_1"),"utf-8");
String membership_main_item = new String(request.getParameter("inputMembershipMainItem").getBytes("8859_1"),"utf-8");
String membership_level = new String(request.getParameter("inputMembershipLevel").getBytes("8859_1"),"utf-8");
String membership_homepage = new String(request.getParameter("inputMembershipHomePage").getBytes("8859_1"),"utf-8");
String membership_photo = new String(request.getParameter("inputMembershipPhoto").getBytes("8859_1"),"utf-8");
java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
String membership_update = formatter.format(new java.util.Date());
Connection myConn = null;
PreparedStatement pstmt = null;
String mySQL = null;
String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
String db_user = "intern";
String db_password = "password";
String Driver="org.mariadb.jdbc.Driver";

try {
     Class.forName(Driver);
     myConn = DriverManager.getConnection(db_url, db_user, db_password);
}catch(SQLException ex) {
         System.err.println("SQLException: " + ex.getMessage());
}

try{
    mySQL = "Update Membership set MEMBERSHIP_NAME=?, MEMBERSHIP_LEVEL=?, HOMEPAGE=?, LOGO_IMAGE=?, TYPE=?, MAIN_ITEM=?, UPDATE_DATE=? where ORG_ID = '" + org_id +"' and MEMBERSHIP_NAME = '"+membership_original_name+"'";
     pstmt = myConn.prepareStatement(mySQL);

     pstmt.setString(1, membership_name);
     pstmt.setString(2, membership_level);
     pstmt.setString(3, membership_homepage);
     pstmt.setString(4, membership_photo);
     pstmt.setString(5, membership_type);
     pstmt.setString(6, membership_main_item);
     pstmt.setString(7, membership_update);

     if(pstmt.executeUpdate() != 0){
%>
<script>
        alert("정보 수정을 완료하였습니다.");
        location.href = "manageMembership.jsp";
</script>
<%
     }
     else{
%>
<script>
        alert("정보 수정에 실패하였습니다.");
        location.href="manaageMembership.jsp";
</script>
<%
     }

  } catch(Exception ex){
        ex.printStackTrace();
        System.err.println("SQLException: " + ex.getMessage());
  }finally{
     myConn.close(); pstmt.close();
  }
%>
