<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>
<%@ include file = "session.jsp"%>

<%
 String event_title = new String(request.getParameter("event_title").getBytes("8859_1"),"utf-8");
 String org_id = (String)session.getAttribute("id");
 String event_type = new String(request.getParameter("inputEventType").getBytes("8859_1"),"utf-8");
 String event_homepage = new String(request.getParameter("inputEventHomePage").getBytes("8859_1"),"utf-8");
 String event_p_d = new String(request.getParameter("inputEventPreRegistrationDate").getBytes("8859_1"),"utf-8");
 String event_p_t = new String(request.getParameter("inputEventPreRegistrationTime").getBytes("8859_1"),"utf-8");
 String event_number = new String(request.getParameter("inputEventNum").getBytes("8859_1"),"utf-8");
 String event_name = new String(request.getParameter("inputEventName").getBytes("8859_1"),"utf-8");
 String event_s_d = new String(request.getParameter("inputEventStartDate").getBytes("8859_1"),"utf-8");
 String event_s_t = new String(request.getParameter("inputEventStartTime").getBytes("8859_1"),"utf-8");
 String event_e_d = new String(request.getParameter("inputEventEndDate").getBytes("8859_1"),"utf-8");
 String event_e_t = new String(request.getParameter("inputEventEndTime").getBytes("8859_1"),"utf-8");
 String event_status = new String(request.getParameter("inputEventState").getBytes("8859_1"),"utf-8");
 String event_order = new String(request.getParameter("inputEventSortingOrder").getBytes("8859_1"),"utf-8");
 String event_host = new String(request.getParameter("inputEventHost").getBytes("8859_1"),"utf-8");
 String event_topic = new String(request.getParameter("inputEventSubject").getBytes("8859_1"),"utf-8");
 String event_detail = new String(request.getParameter("inputEventComment").getBytes("8859_1"),"utf-8");
 String event_supervision = new String(request.getParameter("inputEventManagement").getBytes("8859_1"),"utf-8");
 String event_place = new String(request.getParameter("inputEventPlace").getBytes("8859_1"),"utf-8");
 String event_photo = new String(request.getParameter("inputEventPhoto").getBytes("8859_1"),"utf-8");
 java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
  String event_update = formatter.format(new java.util.Date());
  Connection myConn = null;
  PreparedStatement pstmt = null;
  String mySQL = null;
  String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
  String db_user = "intern";
  String db_password = "password";
  String Driver="org.mariadb.jdbc.Driver";

  try{
    Class.forName(Driver);
    myConn = DriverManager.getConnection(db_url, db_user, db_password);
  }catch(SQLException ex){
      System.err.println("SQLException: " + ex.getMessage());
  }
  try{

      mySQL ="Update Event set TITLE = ?, EVENT_TYPE = ?, EVENT_HOMEPAGE = ?, EVENT_IMAGE = ?, EVENT_S_D = ?, EVENT_S_T = ?, EVENT_E_D = ?, EVENT_E_T = ?, PRE_CLOSING_D = ?, PRE_CLOSING_T = ?, PEOPLE_NUMBER = ?, EVENT_STATUS = ?, GUESTBOOK_ORDER = ?, EVENT_UPDATE = ?, TOPIC = ?, EVENT_HOST = ?, SUPERVISION = ?, EVENT_PLACE = ?, EVENT_DETAIL = ? where ORG_ID = '" + session_id +"' and TITLE = '"+ event_title +"'";
      pstmt = myConn.prepareStatement(mySQL);

       pstmt.setString(1, event_name);
       pstmt.setString(2, event_type);
       pstmt.setString(3, event_homepage);
       pstmt.setString(4, event_photo);
       pstmt.setString(5, event_s_d);
       pstmt.setString(6, event_s_t);
       pstmt.setString(7, event_e_d);
       pstmt.setString(8, event_e_t);
       pstmt.setString(9, event_p_d);
       pstmt.setString(10, event_p_t);
       pstmt.setString(11, event_number);
       pstmt.setString(12, event_status);
       pstmt.setString(13, event_order);
       pstmt.setString(14, event_update);
       pstmt.setString(15, event_topic);
       pstmt.setString(16, event_host);
       pstmt.setString(17, event_supervision);
       pstmt.setString(18, event_place);
       pstmt.setString(19, event_detail);

      if(pstmt.executeUpdate() != 0){
  %>
  <script>
           alert("행사가 수정되었습니다.");
           location.href="manageEventOngoing.jsp";
  </script>
  <%
        }
        else{
   %>
    <script>
             alert("행사 수정이 불가능합니다.");
             location.href="editEvent.jsp";
    </script>
   <%
        }
     } catch(Exception ex){
           ex.printStackTrace();
           System.err.println("SQLException: " + ex.getMessage());
     } finally {
        myConn.close(); pstmt.close();
     }
  %>
