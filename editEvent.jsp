<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>
<%@ include file = "session.jsp"%>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>행사편집 &middot; FORCS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/making.css" rel="stylesheet">
  <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #f5f5f5;
    }

    .form-EventInfo {
      max-width: 900px;
      padding: 0px;
      margin: 20px auto;
      background-color: #fff;
      border: 1px solid #e5e5e5;
      -webkit-border-radius: 5px;
      -moz-border-radius: 5px;
      border-radius: 5px;
      -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }

    .form-EventInfo .form-EventInfo-heading {
      color: white;
      padding: 50px 0;
      margin-top: 0px;
      margin-bottom: 45px;
      background-color: rgb(33, 47, 61);
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
    }

    .form-EventInfo input[type="text"],
    .form-EventInfo input[type="password"],
    .form-EventInfo input[type="file"],
    .form-EventInfo input[type="tel"],
    .form-EventInfo input[type="email"],
    .form-EventInfo input[type="number"],
    .form-EventInfo select {
      font-size: 15px;
      height: auto;
      margin-bottom: 25px;
      padding: 7px 9px;
    }

    .form-EventInfo input[type="date"],
    .form-EventInfo input[type="time"] {
      font-size: 11px;
      margin-bottom: 25px;
      padding: 7px 9px;
    }

    .radio-custom {
      font-size: 28px;
            margin-right: 20px;
    }

    .no-bold {
      font-weight: normal;
    }

  </style>
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>

<body>
  <%
  String event_title = new String(request.getParameter("event_title").getBytes("8859_1"),"utf-8");

  Connection myConn = null;
  Statement stmt = null;
  ResultSet myResultSet;

  String mySQL;
  String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
  String db_user = "intern";
  String db_password = "password";
  String Driver = "org.mariadb.jdbc.Driver";

  String event_type = null;
  String event_homepage = null;
  String pre_closing_date = null;
  String pre_closing_time = null;
  String people_number = null;
  String guestbook_order = null;
  String title = null;
  String event_s_d = null;
  String event_s_t = null;
  String event_e_d = null;
  String event_e_t = null;
  String event_status = null;
  String event_host = null;
  String topic = null;
  String event_detail = null;
  String supervision = null;
  String event_place = null;
  String event_image = null;

  try {
    Class.forName(Driver);
     myConn = DriverManager.getConnection(db_url, db_user, db_password);
     stmt = myConn.createStatement();
  } catch (SQLException ex) {
     System.err.println("SQLException: "+ ex.getMessage());
  }
  mySQL = "select  TITLE, EVENT_TYPE, EVENT_HOMEPAGE, EVENT_IMAGE, EVENT_S_D, EVENT_S_T, EVENT_E_D, EVENT_E_T, PRE_CLOSING_D, PRE_CLOSING_T, PEOPLE_NUMBER, EVENT_STATUS, GUESTBOOK_ORDER, EVENT_UPDATE, TOPIC, EVENT_HOST, SUPERVISION, EVENT_PLACE, EVENT_DETAIL from Event where ORG_ID = '" + session_id +"' and TITLE = '"+ event_title +"'";
  myResultSet = stmt.executeQuery(mySQL);

  if(myResultSet.next()) {

     event_type = myResultSet.getString("EVENT_TYPE");
     event_homepage = myResultSet.getString("EVENT_HOMEPAGE");
     pre_closing_date = myResultSet.getString("PRE_CLOSING_D");
     pre_closing_time = myResultSet.getString("PRE_CLOSING_T");
     people_number = myResultSet.getString("PEOPLE_NUMBER");
     guestbook_order = myResultSet.getString("GUESTBOOK_ORDER");
     title = myResultSet.getString("TITLE");
     event_s_d = myResultSet.getString("EVENT_S_D");
     event_s_t = myResultSet.getString("EVENT_S_T");
     event_e_d = myResultSet.getString("EVENT_E_D");
     event_e_t = myResultSet.getString("EVENT_E_T");
     event_status = myResultSet.getString("EVENT_STATUS");
     event_host = myResultSet.getString("EVENT_HOST");
     topic = myResultSet.getString("TOPIC");
     event_detail = myResultSet.getString("EVENT_DETAIL");
     supervision = myResultSet.getString("SUPERVISION");
     event_place = myResultSet.getString("EVENT_PLACE");
     event_image = myResultSet.getString("EVENT_IMAGE");
   }

  %>
  <div class="container-fluid">
     <form name="editEvent" action="editEvent_verify.jsp?event_title=<%=event_title%>" class="form-EventInfo" method = "post">

          <div class="row center-block">
            <button type="button" class="arrow-btn btn-link" onclick="location.href='manageEventOngoing.jsp'"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></button>
            <h3 class="form-EventInfo-heading text-center">행사 편집</h3>
          </div>

          <div class="row center-block">
            <div class="col-sm-6 border">
              <h4><b>기본 정보</b></h4><span class="space"></span>

              <div class="form-group">
                <label for="inputEventType" class="col-sm-3 control-label col-xs-12"><span class="red-color-text">*</span>행사종류</label>
                <div class="col-sm-9">
                  <select class="form-control" name="inputEventType"  required>
                    <option value="<%=event_type%>"><%=event_type%></option>
                    <option value="정기총회">정기총회</option>
                    <option value="임시총회">임시총회</option>
                    <option value="긴급총회">긴급총회</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="inputEventHomePage" class="col-sm-3 control-label col-xs-12" ><span class="red-color-text">*</span>안내 <br class="hidden-xs" />홈페이지</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputEventHomePage" value="<%=event_homepage%>" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputEventPreRegistration" class="col-sm-3 control-label col-xs-12">
                  <span class="red-color-text">*</span>
                  사전등록 <br class="hidden-xs" />마감일
                </label>
                <div class="col-sm-5">
                  <input type="date" class="form-control" name="inputEventPreRegistrationDate"  value="<%=pre_closing_date%>"  required>
                </div>
                <div class="col-sm-4">
                  <input type="time" class="form-control" name="inputEventPreRegistrationTime"  value="<%=pre_closing_time%>" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputEventNum" class="col-sm-3 control-label col-xs-12">참여 인원</label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" name="inputEventNum"  value="<%=people_number%>">
                </div>
              </div>

            </div>
            <div class="col-sm-6 border">
             <h4 class="hidden-xs">　</h4><span class="space"></span>

             <div class="form-group">
               <label for="inputEventName" class="col-sm-3 control-label col-xs-12"><span class="red-color-text">*</span>행사명</label>
               <div class="col-sm-7 col-xs-10">
                 <input type="text" class="form-control" name="inputEventName" placeholder="행사명" value="<%=title%>" required>
               </div>
               <div class="col-sm-2 col-xs-2">
                 <button type="button" class="btn btn-sm btn-primary space-remove" onclick="check()">중복</button>
               </div>
             </div>

             <div class="form-group">
             <input type="text" class="hidden" name="original" placeholder="기존행사명" value="<%=title%>" required>
             </div>

             <div class="form-group">
               <br class="hidden-xs" />
               <label for="inputEventDate" class="col-sm-3 control-label col-xs-12"><span class="red-color-text">*</span>행사시작일</label>
               <div class="col-sm-5">
                 <input type="date" class="form-control" name="inputEventStartDate" value="<%=event_s_d%>" required>
               </div>
               <div class="col-sm-4">
                 <input type="time" class="form-control" name="inputEventStartTime" value="<%=event_s_t%>" required  >
               </div>
             </div>

             <div class="form-group">
               <br class="hidden-xs" />
               <label for="inputEventDate" class="col-sm-3 control-label col-xs-12"><span class="red-color-text">*</span>행사종료일</label>
               <div class="col-sm-5">
                 <input type="date" class="form-control" name="inputEventEndDate" value="<%=event_e_d%>" required>
               </div>
               <div class="col-sm-4">
                 <input type="time" class="form-control" name="inputEventEndTime" value="<%=event_e_t%>" required>
               </div>
             </div>

             <div class="form-group">
               <label for="inputEventComment" class="col-sm-3 control-label col-xs-12"><span class="red-color-text">*</span>
                 행사상태
               </label>
               <div class=" col-sm-9">

                 <div class="radio-inline">
                   <label class="no-bold">
                   <input type="radio"  value="진행중" name="inputEventState"/> 진행중
                 </label>
                 </div>
                 <div class="radio-inline">
                   <label class="no-bold">
                   <input type="radio" value="완료" name="inputEventState" /> 완료
                 </label>
                 </div>
               </div>
             </div>

            </div>
          </div>

            <div class="col-sm-9 border">
              <div class="form-group">
                <label for="inputEventComment" class="col-sm-2 control-label col-xs-12">
                  방명록 <br class="hidden-xs" />정렬순서
                </label>
                <div class=" col-sm-10">
                  <br class="hidden-xs" />
                  <div class="radio-inline">
                    <label class="no-bold">
                    <input type="radio"  value="option1" name="inputEventSortingOrder" checked> 입력순
                  </label>
                  </div>
                  <div class="radio-inline">
                    <label class="no-bold">
                    <input type="radio" value="option2" name="inputEventSortingOrder" /> 이름 (가나다 순)
                  </label>
                  </div>
                  <div class="radio-inline">
                    <label class="no-bold">
                    <input type="radio" value="option3" name="inputEventSortingOrder" /> 회원사 레벨 & 이름
                  </label>
                  </div>
                </div>
              </div>
            </div>
            <span class="space"></span>

          <div class="row center-block">
            <div class="col-sm-6 border">
              <span class="space"></span>
              <h4><b>추가 정보</b></h4><span class="space"></span>

              <div class="form-group">
                <label for="inputEventHost" class="col-sm-3 control-label col-xs-12">주최</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputEventHost" placeholder="주최" value="<%=event_host%>">
                </div>
              </div>

              <div class="form-group">
                <label for="inputEventSubject" class="col-sm-3 control-label col-xs-12">행사 주제</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputEventSubject" placeholder="행사 주제" value="<%=topic%>">
                </div>
              </div>

              <div class="form-group">
                <label for="inputEventComment" class="col-sm-3 control-label col-xs-12">
                  행사 설명
                </label>
                <div class="col-sm-9">
                  <textarea rows="3" class="form-control form-control-file" name="inputEventComment" ><%=event_detail%></textarea>
                </div>
              </div>

            </div>

            <div class="col-sm-6 border">
              <h4 class="hidden-xs">　</h4><span class="space"></span>

              <div class="form-group">
                <label for="inputEventManagement" class="col-sm-3 control-label col-xs-12">주관</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputEventManagement" placeholder="주관" value="<%=supervision%>">
                </div>
              </div>

              <div class="form-group">
                <label for="inputEventPlace" class="col-sm-3 control-label col-xs-12">행사 장소</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="inputEventPlace" placeholder="행사 장소" value="<%=event_place%>">
                </div>
              </div>

              <div class="form-group">
                <label for="inputEventPhoto" class="col-sm-12 control-label">
                  이미지
                  <div class="help-block pull-right">　(권장사이즈 : 280 * 190, png 파일)</div>
                </label>
                <div class="col-sm-12">
                  <input type="file" class="form-control form-control-file" name="inputEventPhoto" value="<%=event_image%>">
                </div>
              </div>

            </div>
          </div>
          <span class="space"></span>

          <div class="form-group text-center">
            <button type="submit" class="btn btn-primary" onclick="return check2()"> 수정 <i class="fa fa-check"></i></button>
            <span class="space"></span>
              <button type="button" class="btn btn-danger" onclick ="delete_event()"> 행사 삭제 <i class="fa fa-times"></i></button>
          </div>
          <span class="space"></span>
    </form>
  </div>
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap-transition.js"></script>
  <script src="js/bootstrap-alert.js"></script>
  <script src="js/bootstrap-modal.js"></script>
  <script src="js/bootstrap-dropdown.js"></script>
  <script src="js/bootstrap-scrollspy.js"></script>
  <script src="js/bootstrap-tab.js"></script>
  <script src="js/bootstrap-tooltip.js"></script>
  <script src="js/bootstrap-popover.js"></script>
  <script src="js/bootstrap-button.js"></script>
  <script src="js/bootstrap-collapse.js"></script>
  <script src="js/bootstrap-carousel.js"></script>
  <script src="js/bootstrap-typeahead.js"></script>
  <script type="text/javascript">
    var checkValue=0;

    $('input:radio[name=inputEventState]:input[value=<%=event_status%>]').attr("checked", true);
    $('input:radio[name=inputEventSortingOrder]:input[value=<%=guestbook_order%>]').attr("checked", true);

    function delete_event(){
      var devent=document.editEvent.inputEventName.value;
      window.open("delete_event.jsp?inputEventName="+devent,"","width=450 height=135");
      location.href="manageEventOngoing.jsp";
    }

    function check(){
      var event_name = document.getElementsByName("inputEventName")[0].value;
      var original = document.getElementsByName("original")[0].value;
      if(event_name=="") {
        alert("행사명을 입력해주세요.");
      } else {
       var url = "duplicate_event_edit.jsp?inputEventName="+event_name+"&originalEventName="+original;
       window.open(url,"","width=450 height=135");
      }
    }
    function check2(){
      if(checkValue == 0) {
        alert("행사명 중복을 확인해주세요.");
        return false;
      } else  if(checkValue == 1) {
        alert("중복된 행사명입니다. 행사명을 변경해주세요.");
        return false;
      }
    }
    function ok(){
      checkValue = 2;
    }
    function no(){
      checkValue = 1;
    }
  </script>
 </body>
</html>
