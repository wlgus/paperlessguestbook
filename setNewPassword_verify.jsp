<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>﻿

<%
  String org_admin_email = new String(request.getParameter("sessionEmail").getBytes("8859_1"),"utf-8");
  String org_admin_password = new String(request.getParameter("newPassword1").getBytes("8859_1"),"utf-8");
  String org_admin_password2 = new String(request.getParameter("newPassword2").getBytes("8859_1"),"utf-8");
  Connection myConn = null;
  PreparedStatement pstmt = null;
  String mySQL = null;
  String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
  String db_user = "intern";
  String db_password = "password";
  String Driver="org.mariadb.jdbc.Driver";
  try{
    Class.forName(Driver);
    myConn = DriverManager.getConnection(db_url, db_user, db_password);
  }catch(SQLException ex){
      System.err.println("SQLException: " + ex.getMessage());
  }

  if(!org_admin_password.equals(org_admin_password2))
  {
  %>
  <script>
  alert("비밀번호를 체크하세요.");
  location.href="setNewPassword.jsp";
  </script>
  <%
  return;
  }
  try{

    mySQL = "Update Organization set ORG_ADMIN_PASS = ? where ORG_ADMIN_EMAIL = ?";
		pstmt = myConn.prepareStatement(mySQL);

		pstmt.setString(1, org_admin_password);
    pstmt.setString(2, org_admin_email);

		if(pstmt.executeUpdate() != 0){
%>
<script>
			alert("비밀번호가 변경되었습니다.");
			location.href = "signIn.jsp";
</script>
<%
		}

	} catch(Exception ex){
			ex.printStackTrace();
			System.err.println("SQLException: " + ex.getMessage());
	}finally{
		myConn.close(); pstmt.close();
	}
%>
