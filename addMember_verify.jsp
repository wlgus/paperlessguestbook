<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.text.*"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy,jxl.*"%>

<%
int sizeLimit = 10 * 1024 * 1024 ;
String savePath = request.getRealPath("");
String temp = "img";
String realPath = savePath+"/"+temp;
MultipartRequest multi = new MultipartRequest(request, realPath, sizeLimit, "utf-8", new DefaultFileRenamePolicy());
String filename = temp+"/"+multi.getFilesystemName("inputMemberPhoto");

String org_id = (String)session.getAttribute("id");
String membership_name = multi.getParameter("inputMembershipName");
String member_name = multi.getParameter("inputMemberName");
String member_email = multi.getParameter("inputMemberEmail");
String member_phone = multi.getParameter("inputMemberPhone");
String member_tel = multi.getParameter("inputMemberTel");
String member_position = multi.getParameter("inputMemberPosition");
String member_department= multi.getParameter("inputMemberDepartment");

java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
String member_update = formatter.format(new java.util.Date());
Connection myConn = null;
ResultSet myResultSet;
PreparedStatement pstmt = null;
String mySQL = null;
String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
String db_user = "intern";
String db_password = "password";
String Driver="org.mariadb.jdbc.Driver";

try{
	Class.forName(Driver);
	myConn = DriverManager.getConnection(db_url, db_user, db_password);
}catch(SQLException ex){
		System.err.println("SQLException: " + ex.getMessage());
}

try{
	mySQL ="Insert into Member(MEM_EMAIL, MEM_NAME, MEM_DEPARTMENT, MEM_POSITION, MEM_PHONE, MEM_PICTURE, MEM_UPDATE, MEM_TEL, ORG_ID, MEMBERSHIP_NAME) values(?,?,?,?,?,?,?,?,?,?)";

	pstmt = myConn.prepareStatement(mySQL);

	pstmt.setString(1, member_email);
	pstmt.setString(2, member_name);
	pstmt.setString(3, member_department);
	pstmt.setString(4, member_position);
	pstmt.setString(5, member_phone);
	pstmt.setString(6, filename);
	pstmt.setString(7, member_update);
	pstmt.setString(8, member_tel);
	pstmt.setString(9, org_id);
	pstmt.setString(10, membership_name);

	if(pstmt.executeUpdate() != 0){
%>
<script>
			alert("회원가입에 성공하였습니다.");
			location.href = "manageMember.jsp";
</script>
<%
		}
		else{
%>
<script>
			alert("회원가입에 실패하였습니다.");
			location.href="manageMember.jsp";
</script>

<%
		}
	} catch(Exception ex) {
			ex.printStackTrace();
			System.err.println("SQLException: " + ex.getMessage());
	} finally {
		myConn.close(); pstmt.close();
	}
%>
