﻿<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*" %>
<%@ include file = "session.jsp"%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>방명록시스템&middot; FORCS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/making.css" rel="stylesheet">
    <link rel="stylesheet" href="css/stylesheet.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .container-fluid .main {
        max-width: 900px;
        padding: 0px;
        margin: 20px auto;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
      }

      .main .main-heading {
        margin: 0px auto;
        height: 200px;
        color: rgb(255, 255, 255);
        vertical-align: middle;
        text-shadow: 1px 1px 1px rgba(236, 236, 236,0.5);
      }

      .header-div{
        width: 100%;
        height: 100%;
       background-color: rgb(33, 47, 61);
       margin-bottom: 50px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
      }

      .contents{
        margin: 0px 10px 40px;
        height: 235px;
        border: 1px solid #e5e5e5;
        padding: 20px 15px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        background-color: rgba(0, 0, 0, 0.02);
      }

      .navy{
        background-color: rgb(33, 47, 61);
        border: 1px soild rgb(33, 47, 61);
        color: white;
        position: absolute;
        top: 180px;
        right: 45px;
      }

      .img-resizing{
          height: 100px;
          padding: 10px 30px;
          margin-top: 10px;
      }

      .img-resizing2{
          height: 75px;
          padding: 10px 30px;
          margin-top: 10px;
          margin-bottom: 50px;
      }

      .index_tag{
        width: 13px;
        height: 37px;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        position: absolute;
        top: 7px;
        right: 12px;
      }

      .red{  background-color: rgb(194,27,35); }
      .orange{ background-color: rgb(239,67,75); }
      .skyblue{ background-color: rgb(38,154,242); }
      .blue{ background-color: rgb(0,104,208); }


    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
  </head>

  <body>
    <%
     Connection myConn = null;
     Statement stmt = null;
     ResultSet myResultSet;
     String mySQL;
     String db_url = "jdbc:mariadb://192.168.135.126:3306/note";
     String db_user = "intern";
     String db_password = "password";
     String Driver = "org.mariadb.jdbc.Driver";
     String name = null;
     String homepage = null;
     String admin_name = null;
     int num_of_membership = 0;
     int num_of_member = 0;
     int num_of_total_event = 0;
     int num_of_ongoing_event = 0;
     int num_of_finished_event = 0;

       try {
         Class.forName(Driver);
          myConn = DriverManager.getConnection(db_url, db_user, db_password);
          stmt = myConn.createStatement();

       } catch (SQLException ex) {
          System.err.println("SQLException: "+ ex.getMessage());
       }

       mySQL = "select ORG_NAME, ORG_HOMEPAGE, ORG_ADMIN_NAME from Organization where ORG_ID = '" + session_id +"'";
       myResultSet = stmt.executeQuery(mySQL);
       if(myResultSet.next()) {

          name = myResultSet.getString("ORG_NAME");
          homepage = myResultSet.getString("ORG_HOMEPAGE");
          admin_name = myResultSet.getString("ORG_ADMIN_NAME");
        }

        mySQL = "select COUNT(*) from Membership where ORG_ID = '" + session_id +"'";
        myResultSet = stmt.executeQuery(mySQL);
        if(myResultSet.next())
           num_of_membership = myResultSet.getInt(1);

       mySQL = "select COUNT(*) from Member where ORG_ID = '" + session_id +"'";
       myResultSet = stmt.executeQuery(mySQL);
       if(myResultSet.next())
          num_of_member = myResultSet.getInt(1);

        mySQL = "select COUNT(*) from Event where ORG_ID = '" + session_id +"'";
        myResultSet = stmt.executeQuery(mySQL);
        if(myResultSet.next()) {
        num_of_total_event = myResultSet.getInt(1);
         }

       mySQL = "select COUNT(*) from Event where ORG_ID = '" + session_id +"' and EVENT_STATUS = '진행중'";
       myResultSet = stmt.executeQuery(mySQL);
       if(myResultSet.next()) {
       num_of_ongoing_event = myResultSet.getInt(1);
        }

      mySQL = "select COUNT(*) from Event where ORG_ID = '" + session_id +"' and EVENT_STATUS = '완료'";
      myResultSet = stmt.executeQuery(mySQL);
      if(myResultSet.next()) {
      num_of_finished_event = myResultSet.getInt(1);
       }
    %>
    <div class="container-fluid">
      <div class="main">

            <div class="row center-block">
              <div class=" header-div">
              <img src="img/003-agenda-1.png" alt="" class="img-resizing pull-left hidden-xs">
              <img src="img/003-agenda-1.png" alt="" class="img-resizing2 pull-left visible-xs">
              <button type="button" class="btn btn-link pull-right hidden-xs" onclick="location.href='signOut.jsp'">로그아웃 <i class="fa fa-sign-out" aria-hidden="true"></i></button>
              <button type="button" class="btn btn-link pull-right visible-xs" onclick="location.href='signOut.jsp'"><i class="fa fa-sign-out" aria-hidden="true"></i></button>
                <br/>
                <h2 class="main-heading hidden-xs"><b><%=name%><br/>GuestBook System</b></h2>
                <h4 class="main-heading visible-xs"><b><%=name%><br/>GuestBook<br />System</b></h4>
              </div>
            </div>

            <div class="row center-block">

              <div class="col-sm-6">
                <div class="contents">
                  <h4><b>기관 정보</b></h4><hr />
                  <div class="index_tag  red"></div>
                  <p><b>홈페이지</b></p>
                  <div><%=homepage%></div>
                  <div class="space"></div>
                  <p>
                    <b>관리자:</b> <span> <%=admin_name%></span>
                    <div class="space"></div>
                    <button type="button" class="btn btn-sm  navy" onclick="location.href='orgInfo.jsp'"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                  </p>
                </div>
                </div>

              <div class="col-sm-6">
                <div class="contents">
                  <h4><b>행사 관리</b></h4><hr />
                  <div class="index_tag orange"></div>
                  <p>
                    <div><b>전체 행사건수:</b> <span> <%=num_of_total_event%></span> 건 </div><br />
                    <div class="space"></div>
                    <div><b>진행 중: </b> <span> <%=num_of_ongoing_event%></span> 건 / <b> 완료:</b> <span> <%=num_of_finished_event%></span> 건 </div>
                  </p>
                  <button type="button"  class="btn btn-sm  navy" onclick="location.href='manageEventOngoing.jsp'"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                </div>
              </div>

            </div>

            <div class="row center-block">

              <div class="col-sm-6">
                <div class="contents">
                  <h4><b>회원사 관리</b></h4><hr />
                  <div class="index_tag skyblue"></div>
                  <p>
                    <b>회원사 개수: </b><span><%=num_of_membership%></span> 개
                    <div class="space"></div>
                  </p>
                  <button type="button" class="btn btn-sm navy"  onClick="location.href='manageMembership.jsp'" ><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="contents">
                  <h4><b>회원 관리</b></h4><hr />
                  <div class="index_tag blue"></div>
                  <p>
                    <b>회원 수: </b><span><%=num_of_member%></span> 명
                    <div class="space"></div>
                  </p>
                  <button type="button" class="btn btn-sm navy" onClick="location.href='manageMember.jsp'"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                </div>
              </div>

            </div>
      </div>
    </div>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>
  </body>
</html>
